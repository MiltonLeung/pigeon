# <<Pigeon with DB>> is a BLE Master to connect with 2 pigeons at a time and collect motion data from their on-board accelerometer plus gyroscope.
# By Joseph Szeto
# On 2016.12.20

from bluepy.btle import *
import threading
import struct
import time
import re
import logging
import binascii

# Supporting Functions
def _get_localTime():
  return time.strftime("[%Y-%m-%d %H:%M:%S] ", time.localtime())

def _get_localHoursMinutes():
  return time.strftime("%H:%M", time.localtime())

def _P_UUID(val):
  return UUID("%08X-1212-EFDE-1523-785FEABCD123" % (0x00001234 + val))

def _get_MAC(interface):
  # Return the MAC address of interface
  try:
    str = open('/sys/class/net/' + interface + '/address').read()
  except:
    str = "00:00:00:00:00:00"
  return str[0:17]

# Global Variables
HK_ASIA = 8

log = None
logStatus = None
RPi_mac = ""
bt_addrs = ['f5:a1:3d:02:dc:e0', 'c7:0f:5c:10:82:ea', 'df:1b:d0:a8:e8:9c', 'e8:92:1a:47:f9:08', 'cb:92:67:78:dc:e1', 'd2:34:df:29:fe:ae']

servUUID = _P_UUID(0x00)
dataUUID = _P_UUID(0x01)
ctrlUUID = _P_UUID(0x02)

xyz_log = []
connections = []
connection_threads = []
xyz_Lock = threading.Lock()
db_Lock = threading.Lock()
exitFlag = 0
syncFlag = 0

#
class NotificationDelegate(DefaultDelegate):

  def __init__(self, wearable_mac):
    DefaultDelegate.__init__(self)
    self.wearable_mac = wearable_mac
 
  def handleNotification(self, cHandle, data):
    # Get lock
    xyz_Lock.acquire()

    if data[0] == '\x01': # Read Accelerometer Data
      val = struct.unpack("<BhhhhhhhhhB", data)
      A_scale = 8.0 / 32768.0
      A_rate = 1.0 / 50.0
      #
      dataidx = 0
      while (dataidx < 3):
        msg = str(time.time() + A_rate * dataidx) + ', "A", ' + str(val[10]) + ', "' + RPi_mac + '", "' + self.wearable_mac + '", ' + \
        str(tuple([ v*A_scale for v in val ])[dataidx*3+1:dataidx*3+4]).strip('()')
        #log.info(msg)
        xyz_log.append(msg)
        dataidx = dataidx + 1
    elif data[0] == '\x02': # Read Gyroscope Data
      val = struct.unpack("<BhhhhhhhhhB", data)
      G_scale = 500.0 / 32768.0
      G_rate = 1.0 / 50.0
      #
      dataidx = 0
      while (dataidx < 3):
        msg = str(time.time() + G_rate * dataidx) + ', "G", ' + str(val[10]) + ', "' + RPi_mac + '", "' + self.wearable_mac + '", ' + \
        str(tuple([ v*G_scale for v in val ])[dataidx*3+1:dataidx*3+4]).strip('()')
        #log.info(msg)
        xyz_log.append(msg)
        dataidx = dataidx + 1
    elif data[0] == '\x03': # Read Battery Level
      val = struct.unpack("<BBB", data[0:3])
      logStatus.info(_get_localTime() + 'Battery Level of %s' % self.wearable_mac + ' : ' + str(val[2]))
    elif data[0] == '\x06': # Read Active Timetable
      val = struct.unpack("<BBBBBB", data[1:7])
      logStatus.info(_get_localTime() + 'ACTIVE TIME : %s' % self.wearable_mac + '-' + binascii.hexlify(val))
    elif data[0] == '\x08': # Read WB's current time
      val = struct.unpack("<BBBBBB", data[1:7])
      logStatus.info(_get_localTime() + 'Current time : %s' % self.wearable_mac + ' - ' + binascii.hexlify(val))
    # Free lock to release next thread
    xyz_Lock.release()

class ConnectionHandlerThread(threading.Thread):

  def __init__(self, connection_index, bt_mac):
    threading.Thread.__init__(self)
    self.connection_index = connection_index
    self.bt_mac = bt_mac

  def run(self):
    connection = connections[self.connection_index]
    try:
      connection.withDelegate(NotificationDelegate(self.bt_mac))
      service = connection.getServiceByUUID(servUUID)
      ch = service.getCharacteristics(dataUUID)[0]
      ch_CMD = service.getCharacteristics(ctrlUUID)[0]
      # Enable notification
      connection.writeCharacteristic(ch.getHandle()+1, "\x01\x00")
      time.sleep(0.5)
      #
      connection.writeCharacteristic(ch_CMD.getHandle(), "\x02\xCC\x4B\x9B\x01") # Enable Accelerometer & Gyroscope with sampling rate of 52Hz
      time.sleep(0.5)
      # Sync Clock
      ctime = int(round(time.time()))
      ctzone = HK_ASIA * 60
      print binascii.hexlify("\x04\xDD" + struct.pack("<I", ctime) + struct.pack("<h", ctzone))
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x04\xDD" + struct.pack("<I", ctime) + struct.pack("<h", ctzone))
      #time.sleep(0.5)
      # Set Active Time
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x01\xEE\x00\x00\x00\x80\x03\x00")
      #time.sleep(0.5)
      # Read Pigeon's active timetable
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x08\x89")      
      #time.sleep(0.5)
      # Read Pigeon's current time
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x0A\xAB")
      #time.sleep(0.5)      
      # Read battery level
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x03\xCD")
      #time.sleep(0.5)
      # Reset HELP button
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x06\xEF")
      #
      while not exitFlag and connection.waitForNotifications(1.0):
        continue
    except: # Catch all exceptions
      logStatus.info(_get_localTime() + self.bt_mac + ' raised exception during initialization.')

class ScannerThread(threading.Thread):

  def __init__(self, bt_addrs):
    threading.Thread.__init__(self)
    self.bt_addrs = bt_addrs

  def run(self):
    scanner = Scanner(0)
    while not exitFlag:
      devices = scanner.scan(1)
      for d in devices:
        if len(connections) >= 4:
          continue # Skip creating threads
        #
        if d.addr in bt_addrs:
          print 'Pigeon %s (%s), RSSI = %d dB' % (d.addr, d.addrType, d.rssi)
          if d.getValueText(255)[5] == '1':
            print 'Pigeon %s asks for HELP!' % d.addr
          #
          try:
            p = Peripheral(d, ADDR_TYPE_RANDOM)
            service = p.getServiceByUUID(servUUID)
            ch_CMD = service.getCharacteristics(ctrlUUID)[0]
            # Authenticate Pigeon Wristband
            p.writeCharacteristic(ch_CMD.getHandle(), "\x0B\xAB\xCD")
            #
            connections.append(p)
            bt_mac = re.sub(r':', "", d.addr)
            print _get_localTime() + 'Total threads : %d' % len(connections)
            t = ConnectionHandlerThread(len(connections)-1, bt_mac)
            t.start()
            connection_threads.append(t)
            logStatus.info(_get_localTime() + 'Connected with Pigeon ' + d.addr)
          except: # Catch all exceptions
            logStatus.info(_get_localTime() + d.addr + ' raised exception during Scanning.')

class HeartBeatThread(threading.Thread):

  def __init__(self, gw_mac):
    threading.Thread.__init__(self)
    self.gw_mac = gw_mac

  def run(self):
    while not exitFlag:
      # Get DB connection lock
      db_Lock.acquire()
      print _get_localTime() + 'Gateway ' + self.gw_mac + ' is Alive!'
      # Free DB connection lock
      db_Lock.release()
      time.sleep(10)

# Main Thread
def main():
  #
  global log, logStatus, RPi_mac, bt_addrs, exitFlag

  # Initialize Logging to local files
  log = logging.getLogger(__name__)
  log.setLevel(logging.INFO)
  #
  logStatus = logging.getLogger('pnodb-status')
  logStatus.setLevel(logging.INFO)
  # create file handlers
  handler = logging.FileHandler('/home/pi/pigeon.log', mode = 'w')
  handler.setLevel(logging.INFO)
  #
  handlerStatus = logging.FileHandler('/home/pi/pnodb-status.log')
  handlerStatus.setLevel(logging.INFO)
  # add the handlers to the loggers
  log.addHandler(handler)
  logStatus.addHandler(handlerStatus)

  # Read RPi's MAC address
  RPi_mac = re.sub(r':', "", _get_MAC('eth0'))

  # Start heart beat
  h = HeartBeatThread(RPi_mac)
  h.start()

  # Try to initialize and connect with Pigeons nearby (with minimum RSSI?)
  s = ScannerThread(bt_addrs)
  s.start()

  # Main Control Loop
  endTime = time.time() + 60.0
  #
  while time.time() < endTime:
    print _get_localTime() + 'Active Threads : %d' % threading.activeCount()
    time.sleep(1)
  else:
    exitFlag = 1 # Notify threads to exit

  # Upload Motion Data?
  for xyz in xyz_log:
    log.info(xyz)

if __name__ == "__main__":
  main()
