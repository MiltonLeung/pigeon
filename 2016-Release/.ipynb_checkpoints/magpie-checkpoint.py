# <<Magpie>> is an intelligent BLE Gateway that orchestrates all Pigeon Wristbands nearby.
# By Joseph Szeto
# On 2016.11.11

from bluepy.btle import *
import threading
import struct
import MySQLdb
import time
import re
import logging

# Supporting Functions
def _get_localDateTime():
  return time.strftime("[%Y-%m-%d %H:%M:%S] ", time.localtime())

def _get_localTime():
  return time.strftime("%H:%M:%S", time.localtime())

def _P_UUID(val):
  return UUID("%08X-1212-EFDE-1523-785FEABCD123" % (0x00001234 + val))

def _get_MAC(interface):
  # Return the MAC address of interface
  try:
    str = open('/sys/class/net/' + interface + '/address').read()
  except:
    str = "00:00:00:00:00:00"
  return str[0:17]

# Database Connection Parameters
db_host = "192.168.10.132" # Database Server in New Life
#db_host = "10.6.49.84" # Database Server @ ICS
db_user = "ics"
db_pass = "projectARD172"
db_name = "healthcare"

# Global Variables
HK_ASIA = 8

log = None
logStatus = None
RPi_mac = ""
bt_addrs = []

servUUID = _P_UUID(0x00)
dataUUID = _P_UUID(0x01)
ctrlUUID = _P_UUID(0x02)

xyz_log = []
connections = []
connection_threads = []
xyz_Lock = threading.Lock()
db_Lock = threading.Lock()
exitFlag = 0

#
class Mysql_Db:

  def __init__(self, host, user, passcode, database):
    try:
      self.db = MySQLdb.connect(host, user, passcode, database)
    except MySQLdb.Error as e:
      logStatus.info(e)
    else:
      self.cursor = self.db.cursor()

  def select(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      return self.cursor.fetchall()
    except MySQLdb.Error as e:
      return e # To be enhanced

  def selectOne(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      return self.cursor.fetchone()
    except MySQLdb.Error as e:
      return e # To be enhanced

  def insert(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      self.db.commit()
    except MySQLdb.Error as e:
      self.db.rollback()
      logStatus.info(e)

  def disconnect(self):
    self.db.close()

def updateBatteryLevel(wb_mac, level):
  #
  db_Lock.acquire()
  DB = Mysql_Db(db_host, db_user, db_pass, db_name)
  subQuery = '(SELECT wristband_id FROM Wristband WHERE mac_address = "%s")' % wb_mac
  sql = "INSERT INTO BatteryLevel(unix_timestamp, wristband_id, battery_level) VALUES %s" % '(' + str(time.time()) + ', ' + subQuery + ', ' + level + ')'
  DB.insert(sql)
  DB.disconnect()
  # Free DB connection lock
  db_Lock.release()

class NotificationDelegate(DefaultDelegate):

  def __init__(self, wearable_mac):
    DefaultDelegate.__init__(self)
    self.wearable_mac = wearable_mac
    self.bt_mac = re.sub(r':', "", wearable_mac)
 
  def handleNotification(self, cHandle, data):
    # Get lock
    xyz_Lock.acquire()

    if data[0] == '\x01': # Read Accelerometer Data
      val = struct.unpack("<BhhhhhhhhhB", data)
      A_scale = 8.0 / 32768.0
      A_rate = 1.0 / 50.0
      #
      dataidx = 0
      while (dataidx < 3):
        msg = str(time.time() + A_rate * dataidx) + ', "A", ' + str(val[10]) + ', "' + RPi_mac + '", "' + self.bt_mac + '", ' + \
        str(tuple([ v*A_scale for v in val ])[dataidx*3+1:dataidx*3+4]).strip('()')
        #log.info(msg)
        xyz_log.append(msg)
        dataidx = dataidx + 1
    elif data[0] == '\x02': # Read Gyroscope Data
      val = struct.unpack("<BhhhhhhhhhB", data)
      G_scale = 500.0 / 32768.0
      G_rate = 1.0 / 50.0
      #
      dataidx = 0
      while (dataidx < 3):
        msg = str(time.time() + G_rate * dataidx) + ', "G", ' + str(val[10]) + ', "' + RPi_mac + '", "' + self.bt_mac + '", ' + \
        str(tuple([ v*G_scale for v in val ])[dataidx*3+1:dataidx*3+4]).strip('()')
        #log.info(msg)
        xyz_log.append(msg)
        dataidx = dataidx + 1
    elif data[0] == '\x03': # Read Battery Level
      val = struct.unpack("<BBB", data[0:3])
      updateBatteryLevel(self.wearable_mac, str(val[2]))
    elif data[0] == '\x04': # Check ACK
      val = struct.unpack("<BBB", data[1:4])
      logStatus.info(_get_localDateTime() + 'ACK from %s' % self.wearable_mac + ' - ' + str(val[0]) + '-' + str(val[1]) + '-' + str(val[2]))
    elif data[0] == '\x06': # Read Active Timetable
      val = struct.unpack("<BBB", data[1:4])
      print _get_localDateTime() + 'ACTIVE TIME : %s' % self.wearable_mac + '-' + str(val[0]) + '-' + str(val[1]) + '-' + str(val[2])
    elif data[0] == '\x08': # Read WB's current time
      val = struct.unpack("<BBBBBB", data[1:7])
      print _get_localDateTime() + 'Current time : %s' % self.wearable_mac + ' - ' + str(val[0]) + '-' + str(val[1]) + '-' + str(val[2])
    # Free lock to release next thread
    xyz_Lock.release()

class ConnectionHandlerThread(threading.Thread):

  def __init__(self, connection_index, bt_mac):
    threading.Thread.__init__(self)
    self.connection_index = connection_index
    self.bt_mac = bt_mac

  def run(self):
    connection = connections[self.connection_index]
    try:
      connection.withDelegate(NotificationDelegate(self.bt_mac))
      service = connection.getServiceByUUID(servUUID)
      ch = service.getCharacteristics(dataUUID)[0]
      ch_CMD = service.getCharacteristics(ctrlUUID)[0]
      # Enable notification
      connection.writeCharacteristic(ch.getHandle()+1, "\x01\x00")
      time.sleep(0.5)
      #
      connection.writeCharacteristic(ch_CMD.getHandle(), "\x02\xCC\x4A\x9B\x01") # Enable Accelerometer with sampling rate of 52Hz
      time.sleep(0.5)
      # Sync Clock
      ctime = int(round(time.time()))
      ctzone = HK_ASIA * 60
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x04\xDD" + struct.pack("<I", ctime) + struct.pack("<h", ctzone))
      #time.sleep(0.5)
      # Set Active Time
      connection.writeCharacteristic(ch_CMD.getHandle(), "\x01\xEE\xAA\xAA\xAA\xAA\xAA\xAA")
      time.sleep(0.5)
      # Read Pigeon's active timetable
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x08\x89")      
      #time.sleep(0.5)
      # Read Pigeon's current time
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x0A\xAB")
      #time.sleep(0.5)      
      # Read battery level
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x03\xCD")
      #time.sleep(0.5)
      # Reset HELP button
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x06\xEF")
      #
      while not exitFlag and connection.waitForNotifications(1.0):
        if _get_localTime() == '22:55:00':
          # Read battery level
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x03\xCD")
          time.sleep(1.1)
        elif _get_localTime() == '22:55:30':
          # Set Active Time
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x01\xEE\xFF\xFF\xFF\xFF\xFF\xFF")
          time.sleep(1.1)
        continue
    except: # Catch all exceptions
      logStatus.info(_get_localDateTime() + self.bt_mac + ' raised exception during initialization.')

class ScannerThread(threading.Thread):

  def __init__(self, bt_addrs):
    threading.Thread.__init__(self)
    self.bt_addrs = bt_addrs

  def run(self):
    scanner = Scanner(0)
    while not exitFlag:
      devices = scanner.scan(1)
      for d in devices:
        if len(connections) >= 10:
          continue # Skip creating threads
        #
        if d.addr in bt_addrs:
          logStatus.info('Pigeon %s (%s), RSSI = %d dB' % (d.addr, d.addrType, d.rssi))
          if d.getValueText(255)[5] == '1':
            print 'Pigeon %s asks for HELP!' % d.addr
          #
          try:
            p = Peripheral(d, ADDR_TYPE_RANDOM)
            service = p.getServiceByUUID(servUUID)
            ch_CMD = service.getCharacteristics(ctrlUUID)[0]
            # Authenticate Pigeon Wristband
            p.writeCharacteristic(ch_CMD.getHandle(), "\x0B\xAB\xCD")
            #
            connections.append(p)
            #print _get_localDateTime() + 'Total threads : %d' % len(connections)
            t = ConnectionHandlerThread(len(connections)-1, d.addr)
            t.start()
            connection_threads.append(t)
            logStatus.info(_get_localDateTime() + 'Connected with Pigeon ' + d.addr)
          except: # Catch all exceptions
            logStatus.info(_get_localDateTime() + d.addr + ' raised exception during Scanning.')

class HeartBeatThread(threading.Thread):

  def __init__(self, gw_id):
    threading.Thread.__init__(self)
    self.gw_id = gw_id

  def run(self):
    while not exitFlag:
      # Get DB connection lock
      db_Lock.acquire()
      #print _get_localDateTime() + 'Gateway ' + str(self.gw_id) + ' is Alive!'
      DB = Mysql_Db(db_host, db_user, db_pass, db_name)
      sql = "INSERT INTO NodeStatus(unix_timestamp, node_id) VALUES %s" % '(' + str(time.time()) + ', ' + str(self.gw_id) + ')'
      DB.insert(sql)
      DB.disconnect()
      # Free DB connection lock
      db_Lock.release()
      time.sleep(10)

class UploadMotionDataThread(threading.Thread):

  def __init__(self, gw_id):
    threading.Thread.__init__(self)
    self.RPi_id = Rpi_id

  def run(self):
    #time.sleep(self.RPi_id * 20) # Priority according to RPi_id
    time.sleep(self.RPi_id) # Priority according to RPi_id
    # Get DB connection lock
    db_Lock.acquire()
    DB = Mysql_Db(db_host, db_user, db_pass, db_name)
    count = 0
    for xyz in xyz_log:
      if count == 0:
        sql = 'INSERT INTO Motion(unix_timestamp, sensor_type, msg_idx, rpi_id, wristband_id, x, y, z) VALUES ' + '(' + xyz + ')'
      else:
        sql += ',(' + xyz + ')'
      count += 1
      if count > 1000:
        DB.insert(sql)
        count = 0
    #
    if count > 0:
      DB.insert(sql)
    DB.disconnect()
    # Free DB connection lock
    db_Lock.release()
    del xyz_log[:]

# Main Thread
def main():
  #
  global log, logStatus, RPi_mac, bt_addrs, exitFlag

  # Initialize Logging to local files
  log = logging.getLogger(__name__)
  log.setLevel(logging.INFO)
  #
  logStatus = logging.getLogger('ma-status')
  logStatus.setLevel(logging.INFO)
  # create file handlers
  handler = logging.FileHandler('/home/pi/magpie.log', mode = 'w')
  handler.setLevel(logging.INFO)
  #
  handler_S = logging.FileHandler('/home/pi/ma-status.log')
  handler_S.setLevel(logging.INFO)
  # add the handlers to the loggers
  log.addHandler(handler)
  logStatus.addHandler(handler_S)

  # Read RPi's MAC address
  RPi_mac = re.sub(r':', "", _get_MAC('eth0'))

  # Get activated Wristbands' MAC addresses
  DB = Mysql_Db(db_host, db_user, db_pass, db_name)
  sql = "SELECT mac_address FROM Wristband ORDER BY mac_address"
  results = DB.select(sql)
  for row in results:
    bt_addrs.append(row[0])

  # Get RPi's node id
  sql = 'SELECT node_id FROM Node where mac_address = "%s"' % RPi_mac
  row = DB.selectOne(sql)
  RPi_id = row[0]

  # Get active timetable
  sql = 'SELECT active_timetable FROM Timetable where time = "%s"' % _get_localDateTime()[1:12]
  row = DB.selectOne(sql)
  activeTimes = row[0]
  #print activeTimes
  #
  DB.disconnect()

  # Start heart beat
  h = HeartBeatThread(RPi_id)
  h.start()

  # Try to initialize and connect with Pigeons nearby (with minimum RSSI?)
  s = ScannerThread(bt_addrs)
  s.start()

  # Main Thread - that keeps scanning, sending vital signs, managing Connection Threads and doing routine tasks.
  startTime = time.time()
  endTime = startTime + 259200.0
  #
  while time.time() < endTime:
    if _get_localTime() == '23:00:00':
      u = UploadMotionDataThread(RPi_id)
      u.start()
      #exitFlag = 1
    #print _get_localDateTime() + 'Active Threads : %d' % threading.activeCount()
    time.sleep(1)
  else:
    exitFlag = 1 # Notify threads to exit

  '''
  # Copy motion data from file to database
  DB = Mysql_Db(db_host, db_user, db_pass, db_name)
  file = open('/home/pi/magpie.log')
  for line in iter(file):
    sql = "INSERT INTO Motion(unix_timestamp, sensor_type, msg_idx, rpi_id, wristband_id, x, y, z) VALUES %s" % '(' + line + ')'
    #print sql
    DB.insert(sql)
  #
  file.close()
  DB.disconnect()
  '''
if __name__ == "__main__":
  main()
