# <<Magpie>> is an intelligent BLE Gateway that orchestrates all Pigeon Wristbands nearby.
# By Joseph Szeto
# On 2016.11.11

from bluepy.btle import *
import threading
import struct
import MySQLdb
import time
import re
import logging

# Supporting Functions
def _get_localDateTime():
  return time.strftime("[%Y-%m-%d %H:%M:%S] ", time.localtime())

def _get_localTime():
  return time.strftime("%H:%M:%S", time.localtime())

def _P_UUID(val):
  return UUID("%08X-1212-EFDE-1523-785FEABCD123" % (0x00001234 + val))

def _get_MAC(interface):
  # Return the MAC address of interface
  try:
    str = open('/sys/class/net/' + interface + '/address').read()
  except:
    str = "00:00:00:00:00:00"
  return str[0:17]

def _pack_activeTime(timeTable):
  count = 0
  activeByte = []
  while count < 48:
    idx = 0
    value = 0
    while idx < 8:
      if timeTable[count] == '1':
        value += 2 ** idx
      count += 1
      idx += 1
    activeByte.append(value)
  #
  return activeByte

def _pack_cfg(tx, acc, gyro):
  value = 0
  idx = 0
  if gyro == '1':
    value += 2 ** idx
  idx += 1
  if acc == '1':
    value += 2 ** idx
  idx += 1
  count = 2
  while count >= 0:
    if tx[count] == '1':
      value += 2 ** idx
    idx += 1
    count -= 1
  #
  value += 64
  return value

# Database Connection Parameters
db_host = "192.168.10.132" # Database Server in New Life
#db_host = "10.6.49.84" # Database Server @ ICS
db_user = "ics"
db_pass = "projectARD172"
db_name = "healthcare"

# Global Variables
HK_ASIA = 8

log = None
logStatus = None
RPi_mac = ""
bt_addrs = []
timeslots = []
pigeon_cfg = 0

servUUID = _P_UUID(0x00)
dataUUID = _P_UUID(0x01)
ctrlUUID = _P_UUID(0x02)

xyz_log = []
xyz_Lock = threading.Lock()
db_Lock = threading.Lock()
activeSession = 1
exitFlag = 1
timeout = 1

#
class Mysql_Db:

  def __init__(self, host, user, passcode, database):
    try:
      self.db = MySQLdb.connect(host, user, passcode, database)
    except MySQLdb.Error as e:
      logStatus.info(e)
    else:
      self.cursor = self.db.cursor()

  def select(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      return self.cursor.fetchall()
    except MySQLdb.Error as e:
      return e # To be enhanced

  def selectOne(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      return self.cursor.fetchone()
    except MySQLdb.Error as e:
      return e # To be enhanced

  def insert(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      self.db.commit()
    except MySQLdb.Error as e:
      self.db.rollback()
      logStatus.info(e)

  def disconnect(self):
    self.db.close()

def updateBatteryLevel(wb_mac, level):
  # Get DB connection lock
  db_Lock.acquire()
  DB = Mysql_Db(db_host, db_user, db_pass, db_name)
  subQuery = '(SELECT wristband_id FROM Wristband WHERE mac_address = "%s")' % wb_mac
  sql = "INSERT INTO BatteryLevel(unix_timestamp, wristband_id, battery_level) VALUES %s" % '(' + str(time.time()) + ', ' + subQuery + ', ' + level + ')'
  DB.insert(sql)
  DB.disconnect()
  # Free DB connection lock
  db_Lock.release()

def addCalibrationData(wb_mac, data):
  # Get DB connection lock
  db_Lock.acquire()
  DB = Mysql_Db(db_host, db_user, db_pass, db_name)
  subQuery = '(SELECT wristband_id FROM Wristband WHERE mac_address = "%s")' % wb_mac
  sql = "INSERT INTO Calibration(wristband_id, x_offset, y_offset, z_offset) VALUES %s" % '(' + subQuery + ', ' + data + ')'
  DB.insert(sql)
  DB.disconnect()
  # Free DB connection lock
  db_Lock.release()

def getCalibrationData(wb_mac, data):
  DB = Mysql_Db(db_host, db_user, db_pass, db_name)
  subQuery = '(SELECT wristband_id FROM Wristband WHERE mac_address = "%s")' % wb_mac
  sql = "SELECT * FROM Calibration WHERE %s" % 'wristband_id = ' + subQuery
  DB.selectOne(sql)
  DB.disconnect()

class NotificationDelegate(DefaultDelegate):

  def __init__(self, wearable_mac):
    DefaultDelegate.__init__(self)
    self.wearable_mac = wearable_mac
    self.bt_mac = re.sub(r':', "", wearable_mac)
 
  def handleNotification(self, cHandle, data):
    # Get lock
    xyz_Lock.acquire()

    if data[0] == '\x01': # Read Accelerometer Data
      val = struct.unpack("<BhhhhhhhhhB", data)
      A_scale = 8.0 / 32768.0
      A_rate = 1.0 / 50.0
      #
      dataidx = 0
      while (dataidx < 3):
        msg = str(time.time() + A_rate * dataidx) + ', "A", ' + str(val[10]) + ', "' + RPi_mac + '", "' + self.bt_mac + '", ' + \
        str(tuple([ v*A_scale for v in val ])[dataidx*3+1:dataidx*3+4]).strip('()')
        #log.info(msg)
        #print _get_localDateTime() + msg # For debug
        xyz_log.append(msg)
        dataidx = dataidx + 1
    elif data[0] == '\x02': # Read Gyroscope Data
      val = struct.unpack("<BhhhhhhhhhB", data)
      G_scale = 500.0 / 32768.0
      G_rate = 1.0 / 50.0
      #
      dataidx = 0
      while (dataidx < 3):
        msg = str(time.time() + G_rate * dataidx) + ', "G", ' + str(val[10]) + ', "' + RPi_mac + '", "' + self.bt_mac + '", ' + \
        str(tuple([ v*G_scale for v in val ])[dataidx*3+1:dataidx*3+4]).strip('()')
        #log.info(msg)
        xyz_log.append(msg)
        dataidx = dataidx + 1
    elif data[0] == '\x03': # Read Battery Level
      val = struct.unpack("<BBB", data[0:3])
      updateBatteryLevel(self.wearable_mac, str(val[2]))
      #logStatus.info(_get_localDateTime() + 'Battery Level of %s' % self.wearable_mac + ' is ' + str(val[2])) # For debug
    elif data[0] == '\x04': # Check ACK
      if data[1] == '\xAB': # Check authentication response?? Watch Out!
        if data[3] == '\x01':
          logStatus.info(_get_localDateTime() + 'Accepted connection by %s' % self.wearable_mac)
        else:
          logStatus.info(_get_localDateTime() + 'Rejected connection by %s' % self.wearable_mac)
      else:
        val = struct.unpack("<BBB", data[1:4])
        logStatus.info(_get_localDateTime() + 'ACK from %s' % self.wearable_mac + ' - ' + str(val[0]) + '-' + \
        str(val[1]) + '-' + str(val[2]))
    elif data[0] == '\x06': # Read Active Timetable
      val = struct.unpack("<BBBBBB", data[1:7])
      logStatus.info(_get_localDateTime() + 'ACTIVE TIME : %s' % self.wearable_mac + ' - ' + str(val[0]) + '-' + \
      str(val[1]) + '-' + str(val[2]) + '-' + str(val[3]) + '-' + str(val[4]) + '-' + str(val[5]))
    elif data[0] == '\x08': # Read WB's current time
      val = struct.unpack("<BBBBBB", data[1:7])
      logStatus.info(_get_localDateTime() + 'Current time : %s' % self.wearable_mac + ' - ' + str(val[0]) + '-' + \
      str(val[1]) + '-' + str(val[2]) + '-' + str(val[3]) + '-' + str(val[4]) + '-' + str(val[5]))
    elif data[0] == '\x09': # Read WB's calibration data
      val = struct.unpack("<hhh", data[1:7])
      msg = str(val[0]) + ', ' + str(val[1]) + ', ' + str(val[2])
      addCalibrationData(self.wearable_mac, msg)
      #logStatus.info(_get_localDateTime() + 'Calibration Data for %s' % self.wearable_mac + ' is ' + msg) # For debug
    # Free lock to release next thread
    xyz_Lock.release()

class ConnectionHandlerThread(threading.Thread):

  def __init__(self, peripheral, bt_mac):
    threading.Thread.__init__(self)
    self.connection = peripheral
    self.bt_mac = bt_mac

  def run(self):
    global timeslots, pigeon_cfg
    connection = self.connection
    try:
      connection.withDelegate(NotificationDelegate(self.bt_mac))
      service = connection.getServiceByUUID(servUUID)
      ch = service.getCharacteristics(dataUUID)[0]
      ch_CMD = service.getCharacteristics(ctrlUUID)[0]
      # Enable notification
      connection.writeCharacteristic(ch.getHandle()+1, "\x01\x00")
      time.sleep(0.5)
      # Set Pigeon's configuration
      connection.writeCharacteristic(ch_CMD.getHandle(), "\x02\x01" + struct.pack("B", pigeon_cfg) + "\x9B\x01") # set sampling rate @ 52Hz
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x02\x01\x4A\x9B\x01") # For debug
      time.sleep(0.5)
      # Sync Clock
      ctime = int(round(time.time()))
      ctzone = HK_ASIA * 60
      connection.writeCharacteristic(ch_CMD.getHandle(), "\x04\x02" + struct.pack("<I", ctime) + struct.pack("<h", ctzone))
      time.sleep(0.5)
      # Set Active Time
      connection.writeCharacteristic(ch_CMD.getHandle(), "\x01\x03" + \
                                     struct.pack("<BBBBBB", timeslots[0], timeslots[1], timeslots[2], timeslots[3], timeslots[4], timeslots[5]))
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x01\x03\xFF\xFF\xFF\xFF\xFF\xFF") # For debug
      time.sleep(0.5)
      # Read Pigeon's current time
      #connection.writeCharacteristic(ch_CMD.getHandle(), "\x0A\x04")
      #time.sleep(0.5)
      #
      cmd_idx = 4
      while not exitFlag and connection.waitForNotifications(1.0):
        if _get_localTime() == '00:01:30':
          # Read battery level
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x03" + struct.pack("B", cmd_idx))
          time.sleep(1.1)
        elif _get_localTime() == '00:01:32':
          # Get Calibration Data
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x0D\x04")
          time.sleep(1.1)
        elif _get_localTime() == '00:01:34':
          # Sync Clock
          ctime = int(round(time.time()))
          ctzone = HK_ASIA * 60
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x04" + struct.pack("B", cmd_idx) + struct.pack("<I", ctime) + struct.pack("<h", ctzone))
          time.sleep(1.1)
        elif _get_localTime() == '00:01:36':
          # Set Active Time
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x01" + struct.pack("B", cmd_idx) + \
                                         struct.pack("<BBBBBB", timeslots[0], timeslots[1], timeslots[2], timeslots[3], timeslots[4], timeslots[5]))
          time.sleep(1.1)
        elif _get_localTime() == '00:01:38':
          # Read Pigeon's current time
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x0A" + struct.pack("B", cmd_idx))
          time.sleep(1.1)
        elif _get_localTime() == '00:01:40':
          # Reset HELP button ???
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x06" + struct.pack("B", cmd_idx))
          time.sleep(1.1)
        elif _get_localTime() == '00:01:42':
          # Set Configuration
          connection.writeCharacteristic(ch_CMD.getHandle(), "\x02" + struct.pack("B", cmd_idx) + struct.pack("B", pigeon_cfg) + "\x9B\x01") # set sampling rate @ 52Hz
          time.sleep(1.1)
        else:
          continue
        cmd_idx += 1
        if cmd_idx == 171:
          cmd_idx += 1
        if cmd_idx > 255:
          cmd_idx = 0
    except: # Catch all exceptions
      logStatus.info(_get_localDateTime() + self.bt_mac + ' raised exception during initialization.')
    finally:
      connection.disconnect()

class ScannerThread(threading.Thread):

  def __init__(self, bt_addrs):
    threading.Thread.__init__(self)
    self.bt_addrs = bt_addrs

  def run(self):
    logStatus.info(_get_localDateTime() + 'Active Session')
    scanner = Scanner(0)
    while not exitFlag:
      devices = scanner.scan(1)
      for d in devices:
        if d.addr in bt_addrs:
          #logStatus.info('Pigeon %s (%s), RSSI = %d dBm' % (d.addr, d.addrType, d.rssi))
          #if d.getValueText(255)[5] == '1':
          #  logStatus.info(_get_localDateTime() + 'Pigeon %s asks for HELP!' % d.addr)
          #
          if d.rssi < -83:
            #logStatus.info(_get_localDateTime() + 'Signal of Pigeon %s is too weak!' % d.addr)
            continue # skip trying to connect
          #
          try:
            p = Peripheral(d, ADDR_TYPE_RANDOM)
            service = p.getServiceByUUID(servUUID)
            ch_CMD = service.getCharacteristics(ctrlUUID)[0]
            # Authenticate Pigeon Wristband
            p.writeCharacteristic(ch_CMD.getHandle(), "\x0B\xAB\xCD")
            #
            t = ConnectionHandlerThread(p, d.addr)
            t.start()
            logStatus.info(_get_localDateTime() + 'Connected with Pigeon %s @ %ddBm' % (d.addr, d.rssi))
          except: # Catch all exceptions
            logStatus.info(_get_localDateTime() + d.addr + ' raised exception during Scanning.')
    else:
      pass

class TimerThread(threading.Thread):

  def __init__(self, seconds):
    threading.Thread.__init__(self)
    self.seconds = seconds

  def run(self):
    global timeout
    #print 'Timer is started'
    time.sleep(self.seconds)
    timeout = 1

class HeartBeatThread(threading.Thread):

  def __init__(self, gw_id):
    threading.Thread.__init__(self)
    self.gw_id = gw_id

  def run(self):
    while activeSession:
      DB_pulse = Mysql_Db(db_host, db_user, db_pass, db_name)
      sql = "INSERT INTO NodeStatus(unix_timestamp, node_id) VALUES %s" % '(' + str(time.time()) + ', ' + str(self.gw_id) + ')'
      DB_pulse.insert(sql)
      DB_pulse.disconnect()
      time.sleep(10)

class UploadMotionDataThread(threading.Thread):

  def __init__(self, RPi_id):
    threading.Thread.__init__(self)
    self.RPi_id = RPi_id

  def run(self):
    logStatus.info(_get_localDateTime() + 'Inactive Session')
    # Uploading Priority according to RPi_id
    if self.RPi_id <= 20:
      time.sleep(1.0)
    elif self.RPi_id <= 40:
      time.sleep(300.0)
    elif self.RPi_id <= 60:
      time.sleep(600.0)
    else:
      time.sleep(900.0)
    #
    #print _get_localDateTime() + 'xyz_log size : %d.' % len(xyz_log) # For debug
    count = 0
    for xyz in xyz_log:
      if count == 0:
        sql = 'INSERT INTO Motion(unix_timestamp, sensor_type, msg_idx, rpi_id, wristband_id, x, y, z) VALUES ' + '(' + xyz + ')'
      else:
        sql += ',(' + xyz + ')'
      count += 1
      if count >= 10000:
        # Get DB connection lock
        db_Lock.acquire()
        DB = Mysql_Db(db_host, db_user, db_pass, db_name)
        #print sql # For debug
        DB.insert(sql)
        DB.disconnect()
        # Free DB connection lock
        db_Lock.release()
        count = 0
      if activeSession == 1:
        break
    #
    if count > 0:
      # Get DB connection lock
      db_Lock.acquire()
      DB = Mysql_Db(db_host, db_user, db_pass, db_name)
      #print sql # For debug
      DB.insert(sql)
      DB.disconnect()
      # Free DB connection lock
      db_Lock.release()
    #
    del xyz_log[:]

# Main Thread
def main():
  #
  global log, logStatus, RPi_mac, bt_addrs, activeSession, exitFlag, timeout, timeslots, pigeon_cfg

  # Initialize Logging to local files
  log = logging.getLogger(__name__)
  log.setLevel(logging.INFO)
  #
  logStatus = logging.getLogger('ma-status')
  logStatus.setLevel(logging.INFO)
  # create file handlers
  #handler = logging.FileHandler('/home/pi/magpie.log', mode = 'w')
  #handler.setLevel(logging.INFO)
  #
  handler_S = logging.FileHandler('/home/pi/ma-status.log')
  handler_S.setLevel(logging.INFO)
  # add the handlers to the loggers
  #log.addHandler(handler)
  logStatus.addHandler(handler_S)

  # Read RPi's MAC address
  RPi_mac = re.sub(r':', "", _get_MAC('eth0'))

  # Get activated Wristbands' MAC addresses
  DB = Mysql_Db(db_host, db_user, db_pass, db_name)
  sql = "SELECT mac_address FROM Wristband ORDER BY mac_address"
  results = DB.select(sql)
  for row in results:
    bt_addrs.append(row[0])

  # Get RPi's node id
  sql = 'SELECT node_id FROM Node where mac_address = "%s"' % RPi_mac
  row = DB.selectOne(sql)
  RPi_id = row[0]

  # Get active timetable
  sql = 'SELECT active_timetable, transmit_power, accelerometer, gyroscope FROM Timetable where time = "%s"' % _get_localDateTime()[1:12]
  row = DB.selectOne(sql)
  activeTimes = row[0]
  txpower_cfg = row[1]
  acc_cfg = row[2]
  gyro_cfg = row[3]
  #
  #activeTimes = '101010101010101010101011101010101010101010101010' # For debug
  #RPi_id = 1 # For debug

  timeslots = _pack_activeTime(activeTimes)
  pigeon_cfg = _pack_cfg(txpower_cfg, acc_cfg, gyro_cfg)
  logStatus.info(_get_localDateTime() + 'CONFIG : %d *** %d %d %d %d %d %d' % \
                         (pigeon_cfg, timeslots[0], timeslots[1], timeslots[2], timeslots[3], timeslots[4], timeslots[5]))
  DB.disconnect()

  # Main Thread - that keeps scanning, sending vital signs, managing Connection Threads and doing routine tasks.
  powerUp = 1
  while True:
    #print _get_localDateTime() + 'Active Threads : %d *** Timeout : %d' % (threading.activeCount(), timeout)
    if timeout == 1:
      timeout = 0
      if powerUp == 1 or ((_get_localTime()[3:5] == '00' or _get_localTime()[3:5] == '30') and (int(_get_localTime()[6:8]) < 30)):
        powerUp = 0
        hours = int(_get_localTime()[0:2])
        minutes = int(_get_localTime()[3:5])
        if minutes >= 30:
          minutes = 1
        else:
          minutes = 0
        index = hours * 2 + minutes
        #print 'Active Time Index is : %d' % index
        if activeTimes[index] == '0':
          activeSession = 0
          exitFlag = 1 # Notify threads to exit
          u = UploadMotionDataThread(RPi_id)
          u.start()
        else:
          activeSession = 1
          # Start heart beat
          h = HeartBeatThread(RPi_id)
          h.start()
          if exitFlag == 1:
            exitFlag = 0
            # Try to initialize and connect with Pigeons nearby
            s = ScannerThread(bt_addrs)
            s.start()
          else:
            pass
        if _get_localTime()[0:5] == '00:00':
          DB_config = Mysql_Db(db_host, db_user, db_pass, db_name)
          sql = 'SELECT active_timetable, transmit_power, accelerometer, gyroscope FROM Timetable where time = "%s"' % _get_localDateTime()[1:12]
          row = DB_config.selectOne(sql)
          activeTimes = row[0]
          txpower_cfg = row[1]
          acc_cfg = row[2]
          gyro_cfg = row[3]
          #
          timeslots = _pack_activeTime(activeTimes)
          pigeon_cfg = _pack_cfg(txpower_cfg, acc_cfg, gyro_cfg)
          logStatus.info(_get_localDateTime() + 'CONFIG : %d *** %d %d %d %d %d %d' % \
                         (pigeon_cfg, timeslots[0], timeslots[1], timeslots[2], timeslots[3], timeslots[4], timeslots[5]))
          DB_config.disconnect()
        else:
          pass
      # Start Hibernate Timer
      timer = TimerThread(30.0) # To hibernate for 30 seconds
      timer.start()
    #
    time.sleep(1.0)

  '''
  # Copy motion data from file to database
  DB = Mysql_Db(db_host, db_user, db_pass, db_name)
  file = open('/home/pi/magpie.log')
  for line in iter(file):
    sql = "INSERT INTO Motion(unix_timestamp, sensor_type, msg_idx, rpi_id, wristband_id, x, y, z) VALUES %s" % '(' + line + ')'
    #print sql
    DB.insert(sql)
  #
  file.close()
  DB.disconnect()
  '''
if __name__ == "__main__":
  main()
