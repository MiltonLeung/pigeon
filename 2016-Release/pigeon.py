# <<Pigeon>> is a BLE Master to connect with 2 pigeons at a time and collect motion data from their on-board accelerometer plus gyroscope.
# By Joseph Szeto
# On 2016.11.11

from bluepy.btle import *
import threading
import struct
import MySQLdb
import time
import re
import logging

# Supporting Functions
def _get_localTime():
  return time.strftime("[%Y-%m-%d %H:%M:%S] ", time.localtime())

def _get_localHoursMinutes():
  return time.strftime("%H:%M", time.localtime())

def _P_UUID(val):
  return UUID("%08X-1212-EFDE-1523-785FEABCD123" % (0x00001234 + val))

def _get_MAC(interface):
  # Return the MAC address of interface
  try:
    str = open('/sys/class/net/' + interface + '/address').read()
  except:
    str = "00:00:00:00:00:00"
  return str[0:17]

# Database Connection Parameters
#db_host = "192.168.10.132" # Database Server in New Life
db_host = "10.6.49.84" # Database Server @ ICS
db_user = "ics"
db_pass = "projectARD172"
db_name = "healthcare"

# Global Variables
log = None
RPi_mac = ""
bt_addrs = ['f5:a1:3d:02:dc:e0', 'c7:0f:5c:10:82:ea', 'df:1b:d0:a8:e8:9c', 'e8:92:1a:47:f9:08', 'cb:92:67:78:dc:e1', 'd2:34:df:29:fe:ae']

servUUID = _P_UUID(0x00)
dataUUID = _P_UUID(0x01)
ctrlUUID = _P_UUID(0x02)
connections = []
connection_threads = []
db_Lock = threading.Lock()
exitFlag = 0
 
class NotificationDelegate(DefaultDelegate):

  def __init__(self, wearable_mac):
    DefaultDelegate.__init__(self)
    self.wearable_mac = wearable_mac
 
  def handleNotification(self, cHandle, data):
    if data[0] == '\x01': # Read Accelerometer Data
      val = struct.unpack("<BhhhhhhhhhB", data)
      A_scale = 8.0 / 32768.0
      A_rate = 1.0 / 50.0
      #
      dataidx = 0
      while (dataidx < 3):
        msg = str(time.time() + A_rate * dataidx) + ', "A", ' + str(val[10]) + ', "' + RPi_mac + '", "' + self.wearable_mac + '", ' + \
        str(tuple([ v*A_scale for v in val ])[dataidx*3+1:dataidx*3+4]).strip('()')
        log.info(msg)
        dataidx = dataidx + 1
    elif data[0] == '\x02': # Read Gyroscope Data
      val = struct.unpack("<BhhhhhhhhhB", data)
      G_scale = 500.0 / 32768.0
      G_rate = 1.0 / 50.0
      #
      dataidx = 0
      while (dataidx < 3):
        msg = str(time.time() + G_rate * dataidx) + ', "G", ' + str(val[10]) + ', "' + RPi_mac + '", "' + self.wearable_mac + '", ' + \
        str(tuple([ v*G_scale for v in val ])[dataidx*3+1:dataidx*3+4]).strip('()')
        log.info(msg)
        dataidx = dataidx + 1
    elif data[0] == '\x03': # Read Battery Level
      val = struct.unpack("<BBB", data[0:3])
      print 'Battery Level of %s' % self.wearable_mac + ' : ' + str(val[2])

class ConnectionHandlerThread(threading.Thread):

  def __init__(self, connection_index, bt_mac):
    threading.Thread.__init__(self)
    self.connection_index = connection_index
    self.bt_mac = bt_mac

  def run(self):
    connection = connections[self.connection_index]
    try:
      connection.withDelegate(NotificationDelegate(self.bt_mac))
      service = connection.getServiceByUUID(servUUID)
      ch = service.getCharacteristics(dataUUID)[0]
      ch_CMD = service.getCharacteristics(ctrlUUID)[0]
      # Enable notification
      connection.writeCharacteristic(ch.getHandle()+1, "\x01\x00")
      #
      while not exitFlag and connection.waitForNotifications(1.0):
        continue
    except: # Catch all exceptions
      print _get_localTime() + self.bt_mac + ' raised exception'

class HeartBeatThread(threading.Thread):

  def __init__(self, gw_mac):
    threading.Thread.__init__(self)
    self.gw_mac = gw_mac

  def run(self):
    while not exitFlag:
      # Get DB connection lock
      db_Lock.acquire()
      print _get_localTime() + 'Gateway ' + self.gw_mac + ' is Alive!'
      # Free DB connection lock
      db_Lock.release()
      time.sleep(10)

# Main Thread
def main():
  #
  global log, RPi_mac, bt_addrs, exitFlag

  # Initialize Logging to local files
  log = logging.getLogger(__name__)
  log.setLevel(logging.INFO)
  # create file handler
  handler = logging.FileHandler('/home/pi/pigeon.log', mode = 'w')
  handler.setLevel(logging.INFO)
  log.addHandler(handler)

  # Read RPi's MAC address
  RPi_mac = re.sub(r':', "", _get_MAC('eth0'))

  # Start heart beat
  h = HeartBeatThread(RPi_mac)
  h.start()

  # Try to connect with Pigeons nearby (with minimum RSSI?)
  scanner = Scanner(0)
  devices = scanner.scan(10)
  #
  for d in devices:
    if d.addr in bt_addrs:
      try:
        p = Peripheral(d, ADDR_TYPE_RANDOM)
        service = p.getServiceByUUID(servUUID)
        ch_CMD = service.getCharacteristics(ctrlUUID)[0]
        # Initialize Pigeon Wristband
        p.writeCharacteristic(ch_CMD.getHandle(), "\x0B\xAB\xCD") # Start Authentication
        time.sleep(0.5)
        p.writeCharacteristic(ch_CMD.getHandle(), "\x02\xCC\x4B\x9B\x01") # Enable Accelerometer & Gyroscope with sampling rate of 52Hz
        connections.append(p)
        bt_mac = re.sub(r':', "", d.addr)
        t = ConnectionHandlerThread(len(connections)-1, bt_mac)
        t.start()
        connection_threads.append(t)
        print _get_localTime() + 'Connected with Pigeon ' + d.addr
      except: # Catch all exceptions
        print _get_localTime() + d.addr + ' raised exception'

  # Main Control Loop
  cycle = 0
  while cycle < 30:
    print _get_localTime() + 'Active Threads : %d' % threading.activeCount()
    time.sleep(1)
    cycle += 1
  else:
    exitFlag = 1 # Notify threads to exit

  # Copy motion data from file to database
  try:
    DB = MySQLdb.connect(db_host, db_user, db_pass, db_name)
    cursor = DB.cursor()
  except MySQLdb.Error as e:
    print e
  else:
    file = open('/home/pi/pigeon.log')
    for line in iter(file):
      sql = "INSERT INTO Motion(unix_timestamp, sensor_type, msg_idx, rpi_id, wristband_id, x, y, z) VALUES %s" % '(' + line + ')'
      try:
        cursor.execute(sql)
        DB.commit()
      except MySQLdb.Error as e:
        DB.rollback()
        print e
        break
    #
    file.close()
    DB.close()

if __name__ == "__main__":
  main()
