# Pigeon
<<Pigeon>> is an intelligent BLE Gateway that orchestrates all Pigeon Wristbands nearby.  
By Joseph Szeto On 2017.09.25  
Modified By Milton Leung On 2017.10.27
Updated PigeonII By Milton Leung on 2017.12.05

# Installation
The code needs an executeable ```bluepy-helper``` to be compiled from C source. This is done automatically if you use the recommended pip installation method (see below).  
To install the current released verison, on most Debian0based systems:  
```bash
$ sudo apt-get install python-pip libglib2.0-dev python-mysqldb
$ sudo pip install -r requirements.txt
```
For Python3, you may need to use ```pip3```:  
```bash
$ sudo apt-get install python3-pip libglib2.0-dev python3-mysqldb
$ sudo pip3 install -r requirements.txt
```

# Configuration - Pigeon Mac Address
Replace ```wristband_mac_address``` with pigeon corresponding mac address.
```python
bt_addrs = [ 'wristband_mac_address' ]
```

# Configuration - MySQL
```motionLabel.py``` needs to connect MySQL database for data storage and configurations. For instance, the table can be retrieved from ICS server. With external databases, please import database schema from ```database/healthcare_20171102.sql``` and edit db configuration in ```motionLabel.py```:
``` python
db_host="10.6.49.84"
db_user="Username"
db_name="healthcare"
db_pass="Password"
```

# Run

- PigeonI
Directory: pigeonI  
Environment: Python 2.7 / 3.5
```bash
$ sudo python pigeonI/app.py  
```

- PigeonII
Directory: pigeonII 
Environment: Python 2.7 / 3.5
```bash
$ sudo python pigeonII/app.py  

# FAQ
Q: How to start server?  
A: ```sudo python /home/pi/Downloads/pigeonI/app.py```  
Q: How to stop server?  
A: ```sudo pkill python```  
Q: How to get raw data?  
A: ```scp pi@<rpi_ip>:~/Downloads/motion.csv .```  

# TODO
~~- Add support to ECG sensor in future Pigeon II~~
- Integrate pigeon to cloud service
