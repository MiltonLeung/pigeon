from __future__ import (absolute_import, division, print_function, unicode_literals)
from flask import Flask, render_template, request, jsonify
import json
import plotly
from datetime import datetime
from bluepy.btle import *
from collections import deque, defaultdict
import threading
import struct
import time
import numpy as np
from keras.models import load_model
import tensorflow as tf
import logging
# bt_addrs = ['d8:aa:7d:72:18:fb', 'dd:45:6e:0c:ad:9c', '00:0B:57:1C:BB:33']
# bt_addrs = ['da:22:b8:91:b9:57']
bt_addrs = ['da:22:b8:91:b9:57', 'c6:b1:d4:72:4e:68', 'd4:fc:6d:71:b8:12', 'f2:64:ba:df:3d:0e', 'c9:09:d0:ea:eb:66', \
        'd8:01:9a:a7:ec:b6', 'd1:fe:b8:1c:9b:0a', 'fa:2b:62:9a:2f:f2', 'ea:bf:23:fc:8f:d6', 'df:1b:d0:a8:e8:9c']
bt_addrs += ['d7:a6:ca:53:64:1a', 'c9:09:d0:ea:eb:66']
# bt_addrs = ['df:1b:d0:a8:e8:9c']
config_dict={'gyro_rg':{'000':125,'001':245,'010':500,'011':1000,'100':2000},\
        'power':{'000':-30,'001':-20,'010':-16,'011':-12,'100':-8,'101':-4,'110':0,'111':4},\
        'acc':{'0':0,'1':1},'gyro':{'0':0,'1':1},'acc_rg':{'00':2,'01':4,'10':8,'11':16},\
        'gyro_rt':{'000':1e-100,'001':13.0,'010':26.0,'011':52.0,'100':104.0},\
        'acc_rt':{'000':1e-100,'001':13.0,'010':26.0,'011':52.0,'100':104.0}}
connections = []
connection_threads = []
scanner = Scanner(0)
motionList = []
queue = deque(maxlen=1000)
model = load_model('model.h5')
model._make_predict_function()
graph = tf.get_default_graph()
act_class=np.array(['bending','clapping','fanning','static_motion','sit_stand','walking'])

app = Flask(__name__)
logging.basicConfig(format='%(asctime)s%(levelname)s: %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')


def _P_UUID(val):
  return UUID("%08X-1212-EFDE-1523-785FEABCD123" % (0x00001234 + val))

def _get_MAC(interface):
  # Return the MAC address of interface
  try:
    str = open('/sys/class/net/' + interface + '/address').read()
  except:
    str = "00:00:00:00:00:00"
  return str[0:17]

def _unpack_config(c):
    c=''.join(map(lambda x:format(x,'08b'),c))
    return (c[0:3],c[3:6],c[6:7],c[7:8],c[8:10],c[10:13],c[13:16],c[16:18],c[18:24])

RPi_mac = _get_MAC('eth0').replace(':','')

# Notification Delegate Class:
# Receive packages from wristband, handled by connection delegate, 
# further to handle received packages
# Package Format -
# Uplink is defined as data going from the sen sor to server.
# < little endian; B unsigned char (1 byte), h unsigned short (2 bytearray)
# Each uplink packet contains 20 bytes, i.e. 20 B's
# Accelerometer/Gyroscope format - 
# unix_timestamp, sensor_type, msg_idx, rpi_mac, wristband_mac, x, y, z
# For more information, please refer to PlatysensPigeon_v1.10 session 4.3 Uplink Format
class NotificationDelegate(DefaultDelegate):

  def __init__(self, wearable_mac, config):
    DefaultDelegate.__init__(self)
    self.wearable_mac = wearable_mac
    self.gyro_rg,self.power,self.acc,self.gyro,self.acc_rg,self.gyro_rt,self.acc_rt,_,_=\
            _unpack_config(config)
  def _unpack_motion(self, data, sensor):
    timestamp = time.time()
    rg_key,rg_val = ('acc_rg',self.acc_rg) if sensor == "A" else ('gyro_rg',self.gyro_rg)
    rt_key,rt_val = ('acc_rt',self.acc_rt) if sensor == "A" else ('gyro_rt',self.gyro_rt)
    scale = config_dict[rg_key][rg_val] / 32768.0 # Max. val of 2-byte unsigned integer
    rate = 1 / config_dict[rt_key][rt_val] # Sampling rate (Hz)
    val = struct.unpack("<BhhhhhhhhhB", data)
    return [[timestamp + rate * dataidx, sensor,val[10],RPi_mac,self.wearable_mac] \
            +[ v*scale for v in val ][dataidx*3+1:dataidx*3+4] for dataidx in range(3)]
  def handleNotification(self, cHandle, data):
    global queue,motionList
    cmd = struct.unpack("<BBBBBBBBBBBBBBBBBBBB", data)[0]
    if cmd == 1: # Read Accelerometer Data
      msg = self._unpack_motion(data,'A')
      queue+=msg
      motionList+=msg
      # print (msg[-1])
    elif cmd == 2: # Read Gyroscope Data
      msg = self._unpack_motion(data,'G')
      # print ('\n'.join(msg))
    elif cmd == 3: # Read Battery Level
      val = struct.unpack("<BBB", data[0:3])
      print("Device Battery Level 0x03: ",val)
    elif cmd == 4: # Process ACK
      val = struct.unpack("<BBBB", data[0:4])
      print("Acknowledgment 0x04: ",val)
    elif cmd == 5: # Help Request
      val = struct.unpack("<B",data[0])
      print("Help Request 0x05: ", val)
    elif cmd == 6: # Read active time
      val = struct.unpack("<BBBBBBB", data[0:7])
      print("Device Active Time 0x06: ",list(map(lambda x:format(x,'08b'),val)))
    elif cmd == 7: # Read configuration
      val = struct.unpack("<BBBB",data[0:4])
      print("Device Configuration 0x07: ",\
                [(key,config_dict[key][bin]) for (key,bin) in \
                zip(['gyro_rg','power','acc','gyro','acc_rg','gyro_rt','acc_rt'],_unpack_config(val[1:8]))])
    elif cmd == 8: # Read real time
        val = struct.unpack("<BIh",data[0:7])
        print("Device Real Time 0x08: ",val)
    elif cmd == 9: # Read motion calibration offset
        val = struct.unpack("<BBBBBBB",data[0:7])
        print("Device Calibration Data 0x09: ",val)
    else:
        print(cmd)

# Connection Handler Thread: 
# Initialize wristband connection and 
# Further to create NotificationDelegate with
# hex commend to connection handler
# For more information, please refer to PlatysensPigeon_v1.10
# Session 4.5 Downlink Format
class ConnectionHandlerThread(threading.Thread):

  def __init__(self, connection_index, bt_mac, config, timeslots):
    threading.Thread.__init__(self)
    self.connection_index = connection_index
    self.bt_mac = bt_mac
    self.config = config
    self.timeslots = timeslots
  def run(self):
    connection = connections[self.connection_index]
    #
    servUUID = _P_UUID(0x00)
    dataUUID = _P_UUID(0x01)
    ctrlUUID = _P_UUID(0x02)
    try:
      connection.withDelegate(NotificationDelegate(self.bt_mac,self.config))
      service = connection.getServiceByUUID(servUUID)
      ch = service.getCharacteristics(dataUUID)[0]
      ch_CMD = service.getCharacteristics(ctrlUUID)[0]
      # Authenticate Pigeon Wristband
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0BABCD') )
      time.sleep(0.5)
      # Enable notification
      connection.writeCharacteristic(ch.getHandle()+1, bytearray.fromhex('0100') )
      time.sleep(0.5)
      # Sync real time
      ctzone = 8 * 60 # HK_ASIA
      ctime = int(round(time.time())) + ctzone * 60 # shift UTC time by ctzone in second
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0402') + struct.pack("<I", ctime) + struct.pack("<h", ctzone)) 
      time.sleep(0.5)
      # Set Active Time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0104') + struct.pack("<BBBBBB", *self.timeslots)) 
      time.sleep(0.5)
      # Get device active time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0807') )
      time.sleep(0.5)
      # Initialize Pigeon Wristband
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('02DA') + struct.pack("BBB",*self.config)) 
      time.sleep(0.5)
      # Read battery level
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0305') )
      time.sleep(0.5)
      # Set Device Name
      # connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0706') + struct.pack("<s", bytearray("Pigeon".encode()) ))
      # time.sleep(0.5)
      # Get device configuration
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0908') )
      time.sleep(0.5)
      # Get real time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0A09') )
      time.sleep(0.5)
      # Get Calibration Data
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0D0C') )
      time.sleep(0.5)
      # System Reset
      # connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0504') )
      # time.sleep(0.5)
      while True:
        if not connection.waitForNotifications(2.0):
          break # End of thread
    except BTLEException as error:
      print (error)
# timeslots (6 bytes), 0 represents incative session, 1 represents active session
# 0000-0359   0400-0759   0800-1159   1200-1559   1600-1959   2000-2359
# Example:
# 11111111    11111111    11111111    11111111    11111111    11111111
# config (3 bytes)
# gyro_rg power   acc gyr acc_rg  gyro_rt acc_rt  reserve   advertise cyc(s)
# Example: 
# 010     011     1   0      10      001     001     00        000001
class ScannerThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        # timeslots = [0b00000000] * 2 + [0b11111111] * 4
        timeslots = [0b11111111] * 6
        config=[0b01001110,0b10001011,0b00000001]
        while True:
          print('Connected: [' + ','.join(map(lambda t:t.bt_mac,connection_threads)) + ']')
          devices = scanner.scan(2)
          for d in devices:
            # print d.addr
            if d.addr in bt_addrs:
              try:
                p = Peripheral(d, "random")
                connections.append(p)
                bt_mac = d.addr.replace(':','')
                t = ConnectionHandlerThread(len(connections)-1, bt_mac, config, timeslots)
                t.start()
                connection_threads.append(t)
              except BTLEException as error:
                print (error)

class FlaskThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        app.run(host='0.0.0.0', port=8000, debug=True, use_reloader=False)

# File Thread - thread for writing motionList 
class FileThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        global motionList
        while True:
            if len(motionList) > 500:
                outStr='\n'.join(map(lambda x: ','.join(map(str, x)), motionList))+'\n'
                del motionList[:]
                open('/home/pi/Downloads/motion.csv','a').write(outStr)
            time.sleep(5)
        return
        
# Flask routing 
@app.route('/')
def index():
    def init_graph(title):
        layout=dict(
                title='Triaxial Acceleration Plot-'+ title,
                    titlefont=dict(
                        family='Courier New, monospace',
                        size=18,
                        color='#7f7f7f'
                    ),
                    xaxis=dict(
                      title='Time (s)',
                      titlefont=dict(
                        family='Courier New, monospace',
                        size=18,
                        color='#7f7f7f'
                        )
                    ),
                    yaxis=dict(
                        title='Acceleration (9.81 ms-2)',
                            titlefont=dict(
                            family='Courier New, monospace',
                            size=18,
                            color='#7f7f7f',
                        ),
                        range=[-7, 7]
                    ),
                    config=dict(
                        displayModeBar=False
                    )   
                )
        graph=dict(
                data=[
                    dict(
                        x=[],  
                        y=[],
                        name='x'
                    ),dict(
                        x=[],  
                        y=[],
                        name='y'
                    ),dict(
                        x=[],
                        y=[],
                        name='z'
                    )
                ],
                layout=layout
            )
        return graph
    wristband_ids  = sorted(set([ d[4] for d in queue ]))
    graphs = [(wristband_id,init_graph(wristband_id)) for wristband_id in wristband_ids]
    # Add "ids" to each of the graphs to pass up to the client
    # for templating
    ids = ['graph-{}'.format(wristband_id).encode('ascii') for wristband_id,_ in graphs]
    # Convert the figures to JSON
    # PlotlyJSONEncoder appropriately converts pandas, datetime, etc
    # objects to their JSON equivalents
    graphJSON = json.dumps(map(lambda x:x[1],graphs), cls=plotly.utils.PlotlyJSONEncoder)
    return render_template('layouts/index.html', ids=ids, graphJSON=graphJSON)

@app.route('/graph', methods=['POST'])
def update():
    json = request.get_json()

    lst = reversed(list(queue)) # sort by unix_timestamp asc
    graph_list = []
    res = defaultdict(list)
    for v in lst: res[v[4]].append(v) # key: v[4] - wristband_id
    items = sorted(res.items(),key = lambda x: x[0]) # sorted by wristband_id asc
    ids = ['graph-{}'.format(k) for k,_ in items]
    for k,v in items:
        t,sensor_type,msg_id,rpi_id,wristband_id,x,y,z  = zip(*v)
        data = np.asarray([[map(float,d)+[0] for d in zip(x,y,z)[:128]]])
        param={}
        if data.shape == (1,128,4):
            with graph.as_default():
                param={k:str(round(v,2)) for k,v in zip(act_class,model.predict(data)[0])}
                print(param)
        step = float((t[-1]-t[0])/float(len(t)))
        t = [t[0] + i*step for i in range(len(t))]
        toGreen = lambda x:'\x1b[1;31;40m' + x + '\x1b[0m'
        print (datetime.utcfromtimestamp(t[-1]).strftime('%Y-%m-%dT%H:%M:%SZ'),toGreen(wristband_id[-1]))
        graph_list+=[dict(
            x=x,
            y=y,
            z=z,
            t=t,
            param=param
        )]
    return jsonify(dict(graphs=graph_list,ids=ids))

def _misc():
  # x_mean,y_mean,z_mean = map(np.mean,[x_c,y_c,z_c])
  # x_deg = np.arctan(x_mean/(np.sqrt(y_mean**2 + z_mean**2)+1e-50)) / (np.pi /360)
  # y_deg = np.arctan(y_mean/(np.sqrt(x_mean**2 + z_mean**2)+1e-50)) / (np.pi /360)
  # z_deg = np.arctan(np.sqrt(x_mean**2 + y_mean**2)/(z_mean+1e-50)) / (np.pi /360)
  # magnitude = map(lambda ele:np.sqrt(ele[0]**2+ele[1]**2+ele[2]**2), zip(x_c,y_c,z_c))
  # rms=np.sqrt(np.mean(np.square(magnitude)))   
  # corr=np.corrcoef([x_c,y_c,z_c])      
  # corr_xy=corr[1,0]
  # corr_xz=corr[2,0]
  # corr_yz=corr[2,1]
  # crest_factor = max(magnitude)/rms+1e-100
  return
  
def main():
    a = ScannerThread("ScannerThread")
    b = FlaskThread("FlaskThread")
    c = FileThread("FileThread")
    a.start()
    b.start()
    c.start()
    a.join()
    b.join()
    c.join()
if __name__ == "__main__":
    main()

