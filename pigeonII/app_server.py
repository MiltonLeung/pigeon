from __future__ import (absolute_import, division, print_function, unicode_literals)
from flask import Flask, render_template, request, jsonify
import json
import plotly
import plotly.graph_objs as go
from datetime import datetime
from collections import deque, defaultdict
import threading
import struct
import time
from sqlalchemy import *
from keras.models import load_model
import tensorflow as tf
import logging
import numpy as np
from biosppy.signals import ecg
import MySQLdb

class Mysql_Db:

  def __init__(self, host, user, passcode, database):
    try:
      self.db = MySQLdb.connect(host, user, passcode, database)
    except MySQLdb.Error as e:
      logStatus.info(_get_localDateTime() + 'MySQL Error [%d] : %s' % (e.args[0], e.args[1]))
      self.cursor = None
    else:
      self.cursor = self.db.cursor()

  def select(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      return self.cursor.fetchall()
    except MySQLdb.Error as e:
      logStatus.info(_get_localDateTime() + 'MySQL Error [%d] : %s' % (e.args[0], e.args[1]))
      return None

  def selectOne(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      return self.cursor.fetchone()
    except MySQLdb.Error as e:
      logStatus.info(_get_localDateTime() + 'MySQL Error [%d] : %s' % (e.args[0], e.args[1]))
      return None

  def insert(self, sql_stmt):
    try:
      self.cursor.execute(sql_stmt)
      self.db.commit()
    except MySQLdb.Error as e:
      self.db.rollback()
      logStatus.info(_get_localDateTime() + 'MySQL Error [%d] : %s' % (e.args[0], e.args[1]))

  def disconnect(self):
    self.db.close()

DB = Mysql_Db('52.77.240.106', 'root', 'YanASTRIChai', 'pigeon')

# db=create_engine("mysql+pymysql://root:YanASTRIChai@52.77.240.106/pigeon?charset=utf8")
# conn = db.connect()

motion_graph_list = []
# motion_queue=[] # deque(maxlen=1000)
ecg_graph_list = []
ecg_queue= deque(maxlen=2000)
model = load_model('model.h5')
model._make_predict_function()
graph = tf.get_default_graph()

app = Flask(__name__)


class FlaskThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        app.run(host='0.0.0.0', port=8000, debug=True, use_reloader=False)

# File Thread - thread for writing motionList 
class FileThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        global motionList,ecgList
        while True:
            if len(motionList) > 500:
                outStr='\n'.join(map(lambda x: ','.join(map(str, x)), motionList))+'\n'
                del motionList[:]
                open('/home/pi/Downloads/record.csv','a').write(outStr)
            if len(ecgList) > 5000:
                outStr='\n'.join(map(lambda x: ','.join(map(str, x)), ecgList))+'\n'
                del ecgList[:]
                open('/home/pi/Downloads/record.csv','a').write(outStr)

            time.sleep(5)
        return
# Motion Worker - thread for creating output JSON
class MotionWorker(threading.Thread):
    def __init__(self,name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        global motion_graph_list
        while True:
            motion_queue =  list(reversed(DB.select("select id,unix_timestamp, sensor_type, msg_idx, rpi_id, wristband_id, x,y,z from motion order by id desc limit 150;")))
            print(motion_queue[-1])
            res = defaultdict(list)
            graph_list = []
            prediction = dict(x=[[0.0,0.0,0.0,0.0,0.0,0.0]])
            # Initialize Motion ids
            for v in motion_queue: res[v[5]+'-1_motion'].append(v) # key: v[5] - wristband_id with type
            # End Motion Init
            for k,v in res.items():
                 _,t,sensor_type,msg_id,rpi_id,wristband_id,x,y,z  = zip(*v)
                 y = [-_ for _ in y] # Inverse y-axis for fitting model in PigeonII
                 z = [-_ for _ in z] # Inverse z-axis for fitting model in PigeonII
                 idx = 'graph-{}'.format(k)
                 start = t[0]
                 end = t[-1]
                 step = float((end-start)/float(len(t)))
                 t = [t[0] + i*step for i in range(len(t))]
                 data = np.asarray([[map(float,d)+[0] for d in zip(x,y,z)[:128]]])
                 if data.shape == (1,128,4):
                     with graph.as_default():
                         prediction = dict(x=model.predict(data).tolist())
                 graph_list+=[dict(
                     idx=idx,
                     x=x,
                     y=y,
                     z=z,
                     t=t,
                     activity=prediction,
                     act_idx='graph-'+wristband_id[-1]+'-2_activity',
                     param={'crest_factor':0,'rms':0,'corr_xy':0,'corr_xz':0,'corr_yz':0}
                 )]
            motion_graph_list = graph_list
            time.sleep(2)
        return

# ECG Worker: thread for creating output ECG JSON
class ECGWorker(threading.Thread):
    def __init__(self,name):
        threading.Thread.__init__(self)
        self.name = name
    def moving_average(self, a, n=3) :
        ret = np.cumsum(a, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n
    def run(self):
        global ecg_queue,ecg_graph_list
        latest_idx=0
        while True:
            latest_ecg =  [] # reversed(list(db.engine.execute("select id,unix_timestamp, sensor_type, msg_idx, rpi_id, wristband_id, voltage from ecg order by id desc limit 500;")))
            ecg_queue += [l for l in latest_ecg if l[0]>latest_idx]
            latest_idx = 0 if len(ecg_queue) == 0 else list(ecg_queue)[-1][0] # [-1][0] represents largest idx
            res = defaultdict(list)
            graph_list = []
            # Initialize ECG ids
            for v in ecg_queue: res[v[5]+'-3_ecg'].append(v) # key: v[5] - wristband_id with type
            # End ECG Init
            for k,v in res.items():
                _,t,sensor_type,msg_id,rpi_id,wristband_id,vol  = zip(*v)
                idx = 'graph-{}'.format(k)
                start = t[0]
                end = t[-1]
                step = float((end-start)/float(len(t)))
                t = [t[0] + i*step for i in range(len(t))]
                peaks_idx = ecg.hamilton_segmenter(self.moving_average(np.array(vol)), sampling_rate=167.)['rpeaks'].tolist()
                bpm = str(round(166.67/np.average([abs(x-y) for x,y in zip(peaks_idx,peaks_idx[1:])])*60,1))
                peaks_t =  map(lambda x: t[x], peaks_idx)
                peaks = map(lambda x:cache[x],peaks_idx)
                graph_list+=[dict(
                    idx=idx,
                    vol=vol[-500:],
                    t=t[-500:],
                    peaks=peaks,
                    peaks_t=peaks_t,
                    title = 'ECG Plot - Heart Rate: '+bpm+' bpm',
                    param={'crest_factor':0,'rms':0,'corr_xy':0,'corr_xz':0,'corr_yz':0}
                )]
            ecg_graph_list = graph_list
            time.sleep(2)
        return
# Flask routing 
@app.route('/')
def index():
    def init_graph(title):
        get_title_font=lambda :dict(family='Courier New, monospace',size=18,color='#7f7f7f')
        if "motion" in title: #TODO: Specify sensor type instead title hack
            layout=dict(
                    title='Triaxial Acceleration Plot-'+ title.split('-')[0],
                        titlefont=get_title_font(),
                        xaxis=dict(
                          title='Time (s)',
                          titlefont=get_title_font(),
                          ),
                        yaxis=dict(
                            title='Acceleration (9.81 ms-2)',
                            titlefont=get_title_font(),
                            range=(-5,5),
                            config=dict(
                            displayModeBar=False
                        )   
                    )
                )
            graph=dict(
                    data=[
                        dict(
                            x=[],  
                            y=[],
                            name='x'
                        ),dict(
                            x=[],  
                            y=[],
                            name='y'
                        ),dict(
                            x=[],
                            y=[],
                            name='z'
                        )
                    ],
                    layout=layout
                )
        elif 'activity' in title:
            layout=go.Layout(title="Activity Recognition",titlefont=get_title_font(),xaxis=dict(title="probability",titlefont=get_title_font(),range=[0,1]),yaxis=dict(title=""))
            data=[go.Bar(x=[0,0,0,0,0,0],y=['bending','clapping','fanning','static','sit_stand','walking'],orientation='h')]
            graph=dict(data=data,layout=layout)
            return graph
        else: # ECG
            layout=dict(
                    title='ECG Plot-'+ title.split('-')[0],
                    titlefont=get_title_font(),
                    xaxis=dict(
                      title='Time (s)',
                      titlefont=get_title_font()
                    ),
                    yaxis=dict(
                        title=' Voltage (mV)',
                        titlefont=get_title_font(),
                        range=[-500, 500]
                    ),
                    config=dict(
                        displayModeBar=False
                    )   
                )
            graph=dict(
                    data=[
                        dict(
                            x=[],  
                            y=[],
                            name='voltage'
                        ),dict(
                            x=[],  
                            y=[],
                            name='peaks',
                            mode='markers',
                            type='scatter',
                            size=18
                        )
                    ],
                    layout=layout
                )
 
        return graph
    def init_size(wrsitband_id):
        if 'ecg' in wristband_id :
            return "12"
        else:
            return "6"
    wristband_ids = sorted(reduce(lambda x,y:x+y,[[g['idx'],g['act_idx']] if 'motion' in g['idx'] else [g['idx']] for g in motion_graph_list + ecg_graph_list],[]))
    graphs = [(wristband_id,init_graph(wristband_id),init_size(wristband_id)) for wristband_id in wristband_ids]
    # Add "ids" to each of the graphs to pass up to the client
    # for templating
    ids = [[wristband_id.encode('ascii'),size.encode('ascii')] for wristband_id,_,size in graphs]
    # Convert the figures to JSON
    # PlotlyJSONEncoder appropriately converts pandas, datetime, etc
    # objects to their JSON equivalents
    graphJSON = json.dumps(map(lambda x:x[1],graphs), cls=plotly.utils.PlotlyJSONEncoder)
    return render_template('layouts/index.html', ids=ids, graphJSON=graphJSON)

@app.route('/graph', methods=['POST'])
def update():
    return jsonify(dict(graphs=motion_graph_list+ecg_graph_list))

def main():
    b = FlaskThread("FlaskThread")
    d = MotionWorker("MotionWorker")
    e = ECGWorker("ECGWorker")
    b.start()
    d.start()
    e.start()
    b.join()
    d.join()
    e.join()
if __name__ == "__main__":
    main()
