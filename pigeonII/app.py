from __future__ import (absolute_import, division, print_function, unicode_literals)
from flask import Flask, render_template, request, jsonify
import json
import plotly
import plotly.graph_objs as go
from datetime import datetime
from bluepy.btle import *
from collections import deque, defaultdict
import threading
import struct
import time
from keras.models import load_model
import tensorflow as tf
import logging
import numpy as np
from biosppy.signals import ecg
bt_addrs = ['d7:a6:ca:53:64:1a', 'c9:09:d0:ea:eb:66','df:6b:27:b2:88:bc']
temp=['f3:5f:54:a0:ce:99','c2:1f:c9:f2:da:e3']
config_dict={'gyro_rg':{'000':125,'001':245,'010':500,'011':1000,'100':2000},\
        'power':{'000':-30,'001':-20,'010':-16,'011':-12,'100':-8,'101':-4,'110':0,'111':4},\
        'acc':{'0':0,'1':1},'gyro':{'0':0,'1':1},'acc_rg':{'00':2,'01':4,'10':8,'11':16},\
        'gyro_rt':{'000':1e-100,'001':13.0,'010':26.0,'011':52.0,'100':104.0},\
        'acc_rt':{'000':1e-100,'001':13.0,'010':26.0,'011':52.0,'100':104.0}}
connections = []
connection_threads = []
scanner = Scanner(0)
motionList = []
motion_graph_list = []
motion_queue = deque(maxlen=1000)
ecgList = []
ecg_graph_list = []
ecg_queue = deque(maxlen=2000)

model = load_model('model.h5')
model._make_predict_function()
graph = tf.get_default_graph()

app = Flask(__name__)
logging.basicConfig(format='%(asctime)s%(levelname)s: %(message)s', datefmt='%Y-%m-%dT%H:%M:%S')


def _P_UUID(val):
  return UUID("%08X-1212-EFDE-1523-785FEABCD123" % (0x00001234 + val))

def _get_MAC(interface):
  # Return the MAC address of interface
  try:
    str = open('/sys/class/net/' + interface + '/address').read()
  except:
    str = "00:00:00:00:00:00"
  return str[0:17]

def _unpack_config(c):
    c=''.join(map(lambda x:format(x,'08b'),c))
    return (c[0:3],c[3:6],c[6:7],c[7:8],c[8:10],c[10:13],c[13:16],c[16:18],c[18:24])

RPi_mac = _get_MAC('eth0').replace(':','')

# Notification Delegate Class:
# Receive packages from wristband, handled by connection delegate, 
# further to handle received packages
# Package Format -
# Uplink is defined as data going from the sen sor to server.
# < little endian; B unsigned char (1 byte), h unsigned short (2 bytearray)
# Each uplink packet contains 20 bytes, i.e. 20 B's
# Accelerometer/Gyroscope format - 
# unix_timestamp, sensor_type, msg_idx, rpi_mac, wristband_mac, x, y, z
# For more information, please refer to PlatysensPigeon_v1.10 session 4.3 Uplink Format
class NotificationDelegate(DefaultDelegate):

  def __init__(self, wearable_mac, config):
    DefaultDelegate.__init__(self)
    self.wearable_mac = wearable_mac
    self.gyro_rg,self.power,self.acc,self.gyro,self.acc_rg,self.gyro_rt,self.acc_rt,_,_=\
            _unpack_config(config)
  def _unpack_motion(self, data, sensor):
    timestamp = time.time()
    rg_key,rg_val = ('acc_rg',self.acc_rg) if sensor == "A" else ('gyro_rg',self.gyro_rg)
    rt_key,rt_val = ('acc_rt',self.acc_rt) if sensor == "A" else ('gyro_rt',self.gyro_rt)
    scale = config_dict[rg_key][rg_val] / 32768.0 if sensor == "A" else 18.3 / 128.0 # Max. val of 2-byte unsigned integer
    rate = 1 / config_dict[rt_key][rt_val] if sensor == "A" else 1.0 / 512.0 # Sampling rate (Hz)
    val = struct.unpack("<BhhhhhhhhhB", data) if sensor == "A" else struct.unpack(">BhhhhhhhhhB",data)
    if sensor == "E":
        return [[timestamp + rate * dataidx, sensor, val[10], RPi_mac, self.wearable_mac,scale * val[dataidx+1]] for dataidx in range(9) if dataidx % 3 ==0]
    else:
        return [[timestamp + rate * dataidx, sensor,val[10],RPi_mac,self.wearable_mac]+[ v*scale for v in val ][dataidx*3+1:dataidx*3+4] for dataidx in range(3)]
  def handleNotification(self, cHandle, data):
    global motion_queue,ecg_queue,motionList,ecgList
    cmd = struct.unpack("<BBBBBBBBBBBBBBBBBBBB", data)[0]
    if cmd == 1: # Read Accelerometer Data
      msg = self._unpack_motion(data,'A')
      motion_queue+=msg
    elif cmd == 3: # Read ECG Data
      msg = self._unpack_motion(data,'E')
      ecg_queue+=msg
    elif cmd == 4: # Read Battery Level
      val = struct.unpack("<BBB", data[0:3])
      print("Device Battery Level 0x03: ",val)
    elif cmd == 5: # Process ACK
      val = struct.unpack("<BBBB", data[0:4])
      print("Acknowledgment 0x04: ",val)
    elif cmd == 7: # Read active time
      val = struct.unpack("<BBBBBBB", data[0:7])
      print("Device Active Time 0x06: ",list(map(lambda x:format(x,'08b'),val)))
    elif cmd == 8: # Read configuration
      val = struct.unpack("<BBBB",data[0:4])
      print("Device Configuration 0x07: ",\
                [(key,config_dict[key][bin]) for (key,bin) in \
                zip(['gyro_rg','power','acc','gyro','acc_rg','gyro_rt','acc_rt'],_unpack_config(val[1:8]))])
    elif cmd == 9: # Read real time
        val = struct.unpack("<BIh",data[0:7])
        print("Device Real Time 0x08: ",val)
    else:
        print(cmd)

# Connection Handler Thread: 
# Initialize wristband connection and 
# Further to create NotificationDelegate with
# hex commend to connection handler
# For more information, please refer to PlatysensPigeon_v1.10
# Session 4.5 Downlink Format
class ConnectionHandlerThread(threading.Thread):

  def __init__(self, connection_index, bt_mac, config, timeslots):
    threading.Thread.__init__(self)
    self.connection_index = connection_index
    self.bt_mac = bt_mac
    self.config = config
    self.timeslots = timeslots
  def run(self):
    connection = connections[self.connection_index]
    #
    servUUID = _P_UUID(0x00)
    dataUUID = _P_UUID(0x01)
    ctrlUUID = _P_UUID(0x02)
    try:
      connection.withDelegate(NotificationDelegate(self.bt_mac,self.config))
      service = connection.getServiceByUUID(servUUID)
      ch = service.getCharacteristics(dataUUID)[0]
      ch_CMD = service.getCharacteristics(ctrlUUID)[0]
      # Authenticate Pigeon Wristband
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0BABCD') )
      time.sleep(0.5)
      # Enable notification
      connection.writeCharacteristic(ch.getHandle()+1, bytearray.fromhex('0100') )
      time.sleep(0.5)
      # Sync real time
      ctzone = 8 * 60 # HK_ASIA
      ctime = int(round(time.time())) + ctzone * 60 # shift UTC time by ctzone in second
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0402') + struct.pack("<I", ctime) + struct.pack("<h", ctzone)) 
      time.sleep(0.5)
      # Set Active Time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0104') + struct.pack("<BBBBBB", *self.timeslots)) 
      time.sleep(0.5)
      # Get device active time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0807') )
      time.sleep(0.5)
      # Initialize Pigeon Wristband
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('02DA') + struct.pack("BBB",*self.config)) 
      time.sleep(0.5)
      # Read battery level
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0305') )
      time.sleep(0.5)
      # Get device configuration
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0908') )
      time.sleep(0.5)
      # Get real time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0A09') )
      time.sleep(0.5)
      while True:
        if not connection.waitForNotifications(2.0):
          break # End of thread
    except BTLEException as error:
      print (error)
# timeslots (6 bytes), 0 represents incative session, 1 represents active session
# 0000-0359   0400-0759   0800-1159   1200-1559   1600-1959   2000-2359
# Example:
# 11111111    11111111    11111111    11111111    11111111    11111111
# config (3 bytes)
# gyro_rg power   acc ecg acc_rg  ecg_rt acc_rt  reserve   advertise cyc(s)
# Example: 
# 010     011     1   0      10      001     001     00        000001
class ScannerThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        # timeslots = [0b00000000] * 2 + [0b11111111] * 4
        timeslots = [0b11111111] * 6
        config=[0b01001111,0b10001011,0b00000001]
        while True:
          print('Connected: [' + ','.join(map(lambda t:t.bt_mac,connection_threads)) + ']')
          devices = scanner.scan(2)
          for d in devices:
            # print d.addr
            if d.addr in bt_addrs:
              try:
                p = Peripheral(d, "random")
                connections.append(p)
                bt_mac = d.addr.replace(':','')
                t = ConnectionHandlerThread(len(connections)-1, bt_mac, config, timeslots)
                t.start()
                connection_threads.append(t)
              except BTLEException as error:
                print (error)

class FlaskThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        app.run(host='0.0.0.0', port=8000, debug=True, use_reloader=False)

# File Thread - thread for writing motionList 
class FileThread(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        global motionList,ecgList
        while True:
            if len(motionList) > 500:
                outStr='\n'.join(map(lambda x: ','.join(map(str, x)), motionList))+'\n'
                del motionList[:]
                open('/home/pi/Downloads/record.csv','a').write(outStr)
            if len(ecgList) > 5000:
                outStr='\n'.join(map(lambda x: ','.join(map(str, x)), ecgList))+'\n'
                del ecgList[:]
                open('/home/pi/Downloads/record.csv','a').write(outStr)

            time.sleep(5)
        return
# Motion Worker - thread for creating output JSON
class MotionWorker(threading.Thread):
    def __init__(self,name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        global motion_queue,motion_graph_list
        while True:
            res = defaultdict(list)
            graph_list = []
            prediction = []
            # Initialize Motion ids
            lst = reversed(list(motion_queue)) # sort by unix_timestamp asc
            for v in lst: res[v[4]+'-1_motion'].append(v) # key: v[4] - wristband_id with type
            # End Motion Init
            for k,v in res.items():
                 t,sensor_type,msg_id,rpi_id,wristband_id,x,y,z  = zip(*v)
                 y = [-_ for _ in y] # Inverse y-axis for fitting model in PigeonII
                 z = [-_ for _ in z] # Inverse z-axis for fitting model in PigeonII
                 idx = 'graph-{}'.format(k)
                 start = t[0]
                 end = t[-1]
                 step = float((end-start)/float(len(t)))
                 t = [t[0] + i*step for i in range(len(t))]
                 data = np.asarray([[map(float,d)+[0] for d in zip(x,y,z)[:128]]])
                 if data.shape == (1,128,4):
                     with graph.as_default():
                         prediction = model.predict(data).tolist()[0]
                         # prediction = dict(x=[[prediction[i] for i in range(6) if i != 3 and i != 4]]) # Supress 2 unused classes
                         prediction = dict(x=[prediction])
                         # print(prediction)
                 graph_list+=[dict(
                     idx=idx,
                     x=x,
                     y=y,
                     z=z,
                     t=t,
                     activity=prediction,
                     act_idx='graph-'+wristband_id[-1]+'-2_activity',
                     param={'crest_factor':0,'rms':0,'corr_xy':0,'corr_xz':0,'corr_yz':0}
                 )]
            motion_graph_list = graph_list
            time.sleep(0.5)
        return

# ECG Worker: thread for creating output ECG JSON
class ECGWorker(threading.Thread):
    def __init__(self,name):
        threading.Thread.__init__(self)
        self.name = name
    def run(self):
        global ecg_queue,ecg_graph_list
        while True:
            res = defaultdict(list)
            graph_list = []
            # Initialize ECG ids
            lst = reversed(list(ecg_queue)) # sort by unix_timestamp asc
            for v in lst: res[v[4]+'-3_ecg'].append(v) # key: v[4] - wristband_id with type
            # End ECG Init
            for k,v in res.items():
                t,sensor_type,msg_id,rpi_id,wristband_id,vol  = zip(*v)
                idx = 'graph-{}'.format(k)
                start = t[0]
                end = t[-1]
                step = float((end-start)/float(len(t)))
                t = [t[0] + i*step for i in range(len(t))]
                peaks_idx = ecg.hamilton_segmenter(np.array(vol), sampling_rate=167.)['rpeaks'].tolist()
                bpm = str(round(166.67/np.average([abs(x-y) for x,y in zip(peaks_idx,peaks_idx[1:])])*60,1))
                peaks_t = map(lambda x: t[x], peaks_idx)
                peaks = map(lambda x:vol[x],peaks_idx)
                graph_list+=[dict(
                    idx=idx,
                    vol=vol,
                    t=t,
                    peaks=peaks,
                    peaks_t=peaks_t,
                    title = 'ECG Plot - Heart Rate: '+bpm+' bpm',
                    param={'crest_factor':0,'rms':0,'corr_xy':0,'corr_xz':0,'corr_yz':0}
                )]
            ecg_graph_list = graph_list
            time.sleep(0.5)
        return
# Flask routing 
@app.route('/')
def index():
    def init_graph(title):
        get_title_font=lambda :dict(family='Courier New, monospace',size=18,color='#7f7f7f')
        if "motion" in title: #TODO: Specify sensor type instead title hack
            layout=dict(
                    title='Triaxial Acceleration Plot-'+ title.split('-')[0],
                        titlefont=get_title_font(),
                        xaxis=dict(
                          title='Time (s)',
                          titlefont=get_title_font(),
                          ),
                        yaxis=dict(
                            title='Acceleration (9.81 ms-2)',
                            titlefont=get_title_font(),
                            range=(-5,5),
                            config=dict(
                            displayModeBar=False
                        )   
                    )
                )
            graph=dict(
                    data=[
                        dict(
                            x=[],  
                            y=[],
                            name='x'
                        ),dict(
                            x=[],  
                            y=[],
                            name='y'
                        ),dict(
                            x=[],
                            y=[],
                            name='z'
                        )
                    ],
                    layout=layout
                )
        elif 'activity' in title:
            layout=go.Layout(title="Activity Recognition",titlefont=get_title_font(),xaxis=dict(title="probability",titlefont=get_title_font(),range=[0,1]),yaxis=dict(title=""))
            data=[go.Bar(x=[0,0,0,0,0,0],y=['bending','clapping','fanning','static','sit_stand','walking'],orientation='h')]
            graph=dict(data=data,layout=layout)
            return graph
        else: # ECG
            layout=dict(
                    title='ECG Plot-'+ title.split('-')[0],
                    titlefont=get_title_font(),
                    xaxis=dict(
                      title='Time (s)',
                      titlefont=get_title_font()
                    ),
                    yaxis=dict(
                        title=' Voltage (mV)',
                        titlefont=get_title_font(),
                        range=[-500, 500]
                    ),
                    config=dict(
                        displayModeBar=False
                    )   
                )
            graph=dict(
                    data=[
                        dict(
                            x=[],  
                            y=[],
                            name='voltage'
                        ),dict(
                            x=[],  
                            y=[],
                            name='peaks',
                            mode='markers',
                            type='scatter',
                            size=18
                        )
                    ],
                    layout=layout
                )
 
        return graph
    def init_size(wrsitband_id):
        if 'ecg' in wristband_id :
            return "12"
        else:
            return "6"
    wristband_ids = sorted(reduce(lambda x,y:x+y,[[g['idx'],g['act_idx']] if 'motion' in g['idx'] else [g['idx']] for g in motion_graph_list + ecg_graph_list],[]))
    graphs = [(wristband_id,init_graph(wristband_id),init_size(wristband_id)) for wristband_id in wristband_ids]
    # Add "ids" to each of the graphs to pass up to the client
    # for templating
    ids = [[wristband_id.encode('ascii'),size.encode('ascii')] for wristband_id,_,size in graphs]
    # Convert the figures to JSON
    # PlotlyJSONEncoder appropriately converts pandas, datetime, etc
    # objects to their JSON equivalents
    graphJSON = json.dumps(map(lambda x:x[1],graphs), cls=plotly.utils.PlotlyJSONEncoder)
    return render_template('layouts/index.html', ids=ids, graphJSON=graphJSON)

@app.route('/graph', methods=['POST'])
def update():
    return jsonify(dict(graphs=motion_graph_list+ecg_graph_list))

def main():
    a = ScannerThread("ScannerThread")
    b = FlaskThread("FlaskThread")
    # c = FileThread("FileThread")
    d = MotionWorker("MotionWorker")
    e = ECGWorker("ECGWorker")
    a.start()
    b.start()
    # c.start()
    d.start()
    e.start()
    a.join()
    b.join()
    # c.join()
    d.join()
    e.join()
if __name__ == "__main__":
    main()
