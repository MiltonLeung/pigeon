from __future__ import (absolute_import, division, print_function, unicode_literals)
from bluepy.btle import *
import threading
import struct
import time

# bt_addrs = ['d8:aa:7d:72:18:fb', 'dd:45:6e:0c:ad:9c', '00:0B:57:1C:BB:33']
# bt_addrs = ['da:22:b8:91:b9:57']
bt_addrs = ['da:22:b8:91:b9:57', 'c6:b1:d4:72:4e:68', 'd4:fc:6d:71:b8:12', 'f2:64:ba:df:3d:0e', 'c9:09:d0:ea:eb:66', \
        'd8:01:9a:a7:ec:b6', 'd1:fe:b8:1c:9b:0a', 'fa:2b:62:9a:2f:f2', 'ea:bf:23:fc:8f:d6', 'df:1b:d0:a8:e8:9c']
# bt_addrs = ['df:1b:d0:a8:e8:9c']
config_dict={'gyro_rg':{'000':125,'001':245,'010':500,'011':1000,'100':2000},\
        'power':{'000':-30,'001':-20,'010':-16,'011':-12,'100':-8,'101':-4,'110':0,'111':4},\
        'acc':{'0':0,'1':1},'gyro':{'0':0,'1':1},'acc_rg':{'00':2,'01':4,'10':8,'11':16},\
        'gyro_rt':{'000':1e-100,'001':13.0,'010':26.0,'011':52.0,'100':104.0},\
        'acc_rt':{'000':1e-100,'001':13.0,'010':26.0,'011':52.0,'100':104.0}}
connections = []
connection_threads = []
scanner = Scanner(0)

def _P_UUID(val):
  return UUID("%08X-1212-EFDE-1523-785FEABCD123" % (0x00001234 + val))

def _get_MAC(interface):
  # Return the MAC address of interface
  try:
    str = open('/sys/class/net/' + interface + '/address').read()
  except:
    str = "00:00:00:00:00:00"
  return str[0:17]

def _unpack_config(c):
    c=''.join(map(lambda x:format(x,'08b'),c))
    return (c[0:3],c[3:6],c[6:7],c[7:8],c[8:10],c[10:13],c[13:16],c[16:18],c[18:24])

RPi_mac = _get_MAC('eth0').replace(':','')

# Notification Delegate Class:
# Receive packages from wristband, handled by connection delegate, 
# further to handle received packages
# Package Format -
# Uplink is defined as data going from the sen sor to server.
# < little endian; B unsigned char (1 byte), h unsigned short (2 bytearray)
# Each uplink packet contains 20 bytes, i.e. 20 B's
# Accelerometer/Gyroscope format - 
# unix_timestamp, sensor_type, msg_idx, rpi_mac, wristband_mac, x, y, z
# For more information, please refer to PlatysensPigeon_v1.10 session 4.3 Uplink Format
class NotificationDelegate(DefaultDelegate):

  def __init__(self, wearable_mac, config):
    DefaultDelegate.__init__(self)
    self.wearable_mac = wearable_mac
    self.gyro_rg,self.power,self.acc,self.gyro,self.acc_rg,self.gyro_rt,self.acc_rt,_,_=\
            _unpack_config(config)
  def _unpack_motion(self, data, sensor):
    timestamp = time.time()
    rg_key,rg_val = ('acc_rg',self.acc_rg) if sensor == "A" else ('gyro_rg',self.gyro_rg)
    rt_key,rt_val = ('acc_rt',self.acc_rt) if sensor == "A" else ('gyro_rt',self.gyro_rt)
    scale = config_dict[rg_key][rg_val] / 32768.0 # Max. val of 2-byte unsigned integer
    rate = 1 / config_dict[rt_key][rt_val] # Sampling rate (Hz)
    val = struct.unpack("<BhhhhhhhhhB", data)
    return [','.join(map(str,[timestamp + rate * dataidx, sensor,val[10],RPi_mac,self.wearable_mac] \
            +[ v*scale for v in val ][dataidx*3+1:dataidx*3+4])) for dataidx in range(3)]
  def handleNotification(self, cHandle, data):
    cmd = struct.unpack("<BBBBBBBBBBBBBBBBBBBB", data)[0]
    if cmd == 1: # Read Accelerometer Data
      msg = self._unpack_motion(data,'A')
      print ('\n'.join(msg))
    elif cmd == 2: # Read Gyroscope Data
      msg = self._unpack_motion(data,'G')
      print ('\n'.join(msg))
    elif cmd == 3: # Read Battery Level
      val = struct.unpack("<BBB", data[0:3])
      print("Device Battery Level 0x03: ",val)
    elif cmd == 4: # Process ACK
      val = struct.unpack("<BBBB", data[0:4])
      print("Acknowledgment 0x04: ",val)
    elif cmd == 5: # Help Request
      val = struct.unpack("<B",data[0])
      print("Help Request 0x05: ", val)
    elif cmd == 6: # Read active time
      val = struct.unpack("<BBBBBBB", data[0:7])
      print("Device Active Time 0x06: ",list(map(lambda x:format(x,'08b'),val)))
    elif cmd == 7: # Read configuration
      val = struct.unpack("<BBBB",data[0:4])
      print("Device Configuration 0x07: ",\
                [(key,config_dict[key][bin]) for (key,bin) in \
                zip(['gyro_rg','power','acc','gyro','acc_rg','gyro_rt','acc_rt'],_unpack_config(val[1:8]))])
    elif cmd == 8: # Read real time
        val = struct.unpack("<BIh",data[0:7])
        print("Device Real Time 0x08: ",val)
    elif cmd == 9: # Read motion calibration offset
        val = struct.unpack("<BBBBBBB",data[0:7])
        print("Device Calibration Data 0x09: ",val)
    else:
        print(cmd)

# Connection Handler Thread: 
# Initialize wristband connection and 
# Further to create NotificationDelegate with
# hex commend to connection handler
# For more information, please refer to PlatysensPigeon_v1.10
# Session 4.5 Downlink Format
class ConnectionHandlerThread(threading.Thread):

  def __init__(self, connection_index, bt_mac, config, timeslots):
    threading.Thread.__init__(self)
    self.connection_index = connection_index
    self.bt_mac = bt_mac
    self.config = config
    self.timeslots = timeslots
  def run(self):
    connection = connections[self.connection_index]
    #
    servUUID = _P_UUID(0x00)
    dataUUID = _P_UUID(0x01)
    ctrlUUID = _P_UUID(0x02)
    try:
      connection.withDelegate(NotificationDelegate(self.bt_mac,self.config))
      service = connection.getServiceByUUID(servUUID)
      ch = service.getCharacteristics(dataUUID)[0]
      ch_CMD = service.getCharacteristics(ctrlUUID)[0]
      # Authenticate Pigeon Wristband
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0BABCD') )
      time.sleep(0.5)
      # Enable notification
      connection.writeCharacteristic(ch.getHandle()+1, bytearray.fromhex('0100') )
      time.sleep(0.5)
      # Set Active Time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0104') + struct.pack("<BBBBBB", *self.timeslots)) 
      time.sleep(0.5)
      # Initialize Pigeon Wristband
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('02DA') + struct.pack("BBB",*self.config)) 
      time.sleep(0.5)
      # Read battery level
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0305') )
      time.sleep(0.5)
      # Sync real time
      ctime = int(round(time.time()))
      ctzone = 8 * 60 # HK_ASIA
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0402') + struct.pack("<I", ctime) + struct.pack("<h", ctzone)) 
      time.sleep(0.5)
      # Set Device Name
      # connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0706') + struct.pack("<s", bytearray("Pigeon".encode()) ))
      # time.sleep(0.5)
      # Get device active time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0807') )
      time.sleep(0.5)
      # Get device configuration
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0908') )
      time.sleep(0.5)
      # Get real time
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0A09') )
      time.sleep(0.5)
      # Get Calibration Data
      connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0D0C') )
      time.sleep(0.5)
      # System Reset
      # connection.writeCharacteristic(ch_CMD.getHandle(), bytearray.fromhex('0504') )
      # time.sleep(0.5)
      while True:
        if not connection.waitForNotifications(2.0):
          break # End of thread
        
    except BTLEException as error:
      print (error)

# timeslots (6 bytes), 0 represents incative session, 1 represents active session
# 0000-0359   0400-0759   0800-1159   1200-1559   1600-1959   2000-2359
# Example:
# 11111111    11111111    11111111    11111111    11111111    11111111
# config (3 bytes)
# gyro_rg power   acc gyr acc_rg  gyro_rt acc_rt  reserve   advertise cyc(s)
# Example: 
# 010     011     1   0      10      001     001     00        000001
def main():
  timeslots = [0b11111111] * 6 
  # timeslots = [0b00000000] * 6 # Turn off all active timeslots
  config=[0b01001110,0b10001001,0b00000001]
  while True:
    print ('Connected: ' + ','.join(map(lambda t:t.bt_mac,connection_threads)))
    print ('Scanning...')
    devices = scanner.scan(2)
    for d in devices:
      # print d.addr
      if d.addr in bt_addrs:
        try:
          p = Peripheral(d, "random")
          connections.append(p)
          bt_mac = d.addr.replace(':','')
          t = ConnectionHandlerThread(len(connections)-1, bt_mac, config,timeslots)
          t.start()
          connection_threads.append(t)
        except BTLEException as error:
          print (error)

if __name__ == "__main__":
    main()
